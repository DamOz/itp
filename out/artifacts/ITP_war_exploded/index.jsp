<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<head>
    <title>Instant Training Plan - home page</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, inital-scale=1, shrink-to-fit=no">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Lato:wght@300;400;700;900&family=Oswald:wght@200;300;400;500;700&display=swap"
          rel="stylesheet">
    <link rel="stylesheet" href="./resources/style.css">
</head>
<body>
<!-- navigation -->

    <jsp:include page="fragment/navbar.jsp"/>

<!--Welcome info -->
<section>
    <div class="container-fluid">
        <h1>Witaj na stronie ąę</h1>

    </div>
</section>
<!--footer-->

<jsp:include page="fragment/footer.jsp"/>

</body>
</html>
