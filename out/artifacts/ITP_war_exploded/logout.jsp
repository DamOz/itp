<%--
  Created by IntelliJ IDEA.
  User: Admin
  Date: 2021-05-09
  Time: 10:44
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!doctype html>
<html lang="pl">
<head>
    <title>Instant Training Plan - logout</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, inital-scale=1, shrink-to-fit=no">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Lato:wght@300;400;700;900&family=Oswald:wght@200;300;400;500;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="./resources/style.css">
</head>
<body>
<!-- navigation -->

<jsp:include page="fragment/navbar.jsp"/>

<!--Welcome info -->
<section>
    <div class="col text-center mb-5">
        <form class="col-sm-8 col-md-6 col-lg-4 mx-auto d-block " method="post" action="login">
            <h1 class="mb-3 text-danger">Zostałeś wylogowany</h1>
            <h3>Chcesz wrócić na stronę główną?</h3>
            <a href="${pageContext.request.contextPath}/" class="btn btn-lg btn-block mb-3">Strona główna</a>
            <p class="mt-5 mb-3">© InstantTrainingPlan 2021</p>
        </form>
    </div>
</section>
<!--footer-->

<jsp:include page="fragment/footer.jsp"/>

</body>
</html>
