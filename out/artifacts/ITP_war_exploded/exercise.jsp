<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Exercise</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, inital-scale=1, shrink-to-fit=no">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Lato:wght@300;400;700;900&family=Oswald:wght@200;300;400;500;700&display=swap"
          rel="stylesheet">
    <link rel="stylesheet" href="./resources/style.css">
</head>
<body>
<!-- navigation -->

<jsp:include page="fragment/navbar.jsp"/>


<div class="container">
    <div class="col-sm-6 col-md-4 col-md-offset-4">
        <form class="form-signin" method="post" action="exercise">
            <h2 class="form-signin-heading">Dodaj nowe ćwiczenie</h2>
            <label>Nazwa ćwiczenia</label>
            <input name="inputName" type="text" class="form-control" placeholder="NAZWA CWICZENIA" required/>
            <label>Na jaką partię?</label>
            <select name="inputBodypart" class="custom-select" required>
                <option value="1">czworoglowy</option>
                <option value="2">dwugłowy</option>
                <option value="3">łydka</option>
                <option value="4">piszelowe</option>
                <option value="5">przywodziciele</option>
                <option value="6">pośladek</option>
                <option value="7">pośladek odwodziciel</option>
                <option value="8">prostowniki</option>
                <option value="9">najszerszy dół</option>
                <option value="10">najszerszy góra</option>
                <option value="11">kaptury</option>
                <option value="12">barki przód</option>
                <option value="13">barki środek</option>
                <option value="14">barki tył</option>
                <option value="15">biceps</option>
                <option value="16">triceps</option>
                <option value="17">klatka dół</option>
                <option value="18">klatka góra</option>
                <option value="19">brzuch góra</option>
                <option value="20">brzuch dół</option>
                <option value="21">brzuch skosy</option>
                <option value="22">całe ciało</option>
                <option value="23">skakanie</option>
            </select>
            <label>Poziom trudności?</label>
            <select name="inputDif" class="custom-select" required>
                <option value="1">łatwe</option>
                <option value="2">średnie</option>
                <option value="3">zaawansowane</option>
                <option value="4">trudne</option>
                <option value="5">bardzo trudne</option>
            </select>
            <label>Czy tylko wykonasz na siłowni?</label>
            <select name="inputGym" class="custom-select" required>
                <option value="0">Tak</option>
                <option value="1">Nie</option>
            </select>
            <label>Jakie przyrząd jest wymagany?</label>
            <select name="inputItem" class="custom-select" required>
                <option value="1">body</option>
                <option value="2">TRX</option>
                <option value="3">kettle</option>
                <option value="4">dumbell</option>
                <option value="5">bar</option>
                <option value="6">bosu</option>
                <option value="7">guma</option>
            </select>
            <label>Jakie kontuzje dotyczą?</label>
            <select name="inputInjury" class="custom-select" required>
                <option value="1">szyja</option>
                <option value="2">barki</option>
                <option value="3">łokcie</option>
                <option value="4">biodra</option>
                <option value="5">kolana</option>
                <option value="6">kostki</option>
            </select>
            <button class="btn btn-lg btn-primary btn-block" type="submit">Dodaj ćwiczenie</button>
        </form>
    </div>
</div>
<!--footer-->

<jsp:include page="fragment/footer.jsp"/>

</body>
</html>
