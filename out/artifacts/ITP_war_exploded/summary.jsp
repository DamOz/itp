<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!doctype html>
<html lang="pl">
<head>
    <title>Instant Training Plan - summary</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, inital-scale=1, shrink-to-fit=no">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Lato:wght@300;400;700;900&family=Oswald:wght@200;300;400;500;700&display=swap"
          rel="stylesheet">
    <link rel="stylesheet" href="./resources/style.css">
</head>
<body>
<!-- navigation -->

<jsp:include page="fragment/navbar.jsp"/>

<!--Welcome info -->
<section>
    <div class="container">
        <div class="col-sm-2 col-md-8 col-md-offset-4">
                <c:set var="personal" value="${requestScope.personal}"></c:set>
                <c:set var="conv" value="${requestScope.conv}"></c:set>
                <h2 class="form-signin-heading">Potwierdź podane informacje</h2>
                <div class="row">
                    <div class="col-8"><label>Twój cel treningu:</label></div>
                    <div class="col-4"><p><c:out value="${conv.personalGoalConv(personal.goal)}"/> </p></div>
                </div>
                <div class="row">
                    <div class="col-8"><label>Twoje treningowe doświadczenie: </label>
                    </div>
                    <div class="col-4"><p><c:out value="${conv.personalExpConv(personal.exp)}"/> </p></div>
                </div>
                <div class="row">
                    <div class="col-8"><label>Ważysz:</label></div>
                    <div class="col-4"><p><c:out value="${personal.weight}"/> kg</p></div>
                </div>
                <div class="row">
                    <div class="col-8"><label>Chcesz w tygodniu trenować: </label></div>
                    <div class="col-4"><p><c:out value="${personal.training}"/> razy</p></div>
                </div>
                <h3>Podałeś urazy, które trzeba wziąć pod uwagę przy generowaniu planu.</h3>
                <div class=" displey-b">
                    <div class="row">
                        <div class="col-10"><label>Kark/szyja/odcinek szyjny kręgosłupa</label></div>
                        <div class="col-2"><p><c:out value="${conv.personalInjuryConv(personal.neck)}"/> </p></div>
                    </div>
                    <div class="row">
                        <div class="col-10"><label>Barki/Stawy ramienne</label></div>
                        <div class="col-2"><p><c:out value="${conv.personalInjuryConv(personal.shoulder)}"/> </p></div>
                    </div>

                    <div class="row">
                        <div class="col-10"><label>Łokcie/Stawy łokciowe</label></div>
                        <div class="col-2"><p><c:out value="${conv.personalInjuryConv(personal.elbow)}"/> </p></div>
                    </div>
                    <div class="row">
                        <div class="col-10"><label>Biodra/Stawy bioderowe</label></div>
                        <div class="col-2"><p><c:out value="${conv.personalInjuryConv(personal.hips)}"/> </p></div>
                    </div>
                    <div class="row">
                        <div class="col-10"><label>Kolana/Stawy kolanowe</label></div>
                        <div class="col-2"><p><c:out value="${conv.personalInjuryConv(personal.knees)}"/> </p></div>
                    </div>
                    <div class="row">
                        <div class="col-10"><label>Kostki/Stawy skokowe</label></div>
                        <div class="col-2"><p><c:out value="${conv.personalInjuryConv(personal.ankles)}"/> </p></div>
                    </div>
                    <h3>Masz dostęp do:</h3>
                    <div class="row">
                        <div class="col-10"><label>Siłownia</label></div>
                        <div class="col-2"><p><c:out value="${conv.personalGymConv(personal.gym)}"/> </p></div>
                    </div>
                    <div class="row">
                        <div class="col-10"><label>Będę ćwiczył bez przyrządów.</label></div>
                        <div class="col-2"><p><c:out value="${conv.personalBodyConv(personal.body)}"/> </p></div>
                    </div>
                    <div class="row">
                        <div class="col-10"><label>Pasy typu TRX</label></div>
                        <div class="col-2"><p><c:out value="${conv.personalItemConv(personal.trx)}"/> </p></div>
                    </div>
                    <div class="row">
                        <div class="col-10"><label>Obciążniki kettlebells</label></div>
                        <div class="col-2"><p><c:out value="${conv.personalItemConv(personal.kettle)}"/> </p></div>
                    </div>
                    <div class="row">
                        <div class="col-10"><label>Ciężarki/Hantle różnej wagi</label></div>
                        <div class="col-2"><p><c:out value="${conv.personalItemConv(personal.dumb)}"/> </p></div>
                    </div>
                    <div class="row">
                        <div class="col-10"><label>Drążek do podciągania</label></div>
                        <div class="col-2"><p><c:out value="${conv.personalItemConv(personal.bar)}"/> </p></div>
                    </div>
                    <div class="row">
                        <div class="col-10"><label>BOSU</label></div>
                        <div class="col-2"><p><c:out value="${conv.personalItemConv(personal.bosu)}"/> </p></div>
                    </div>
                    <div class="row">
                        <div class="col-10"><label>Taśmy/Gumy do ćwiczeń</label></div>
                        <div class="col-2"><p><c:out value="${conv.personalItemConv(personal.gum)}"/> </p></div>
                    </div>
                </div>

                <a href="${pageContext.request.contextPath}/training" class="btn btn-lg btn-primary">Generuj trening
                </a>

        </div>
    </div>
</section>
<!--footer-->

<jsp:include page="fragment/footer.jsp"/>

</body>
</html>
