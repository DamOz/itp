<%--
  Created by IntelliJ IDEA.
  User: Admin
  Date: 2021-05-09
  Time: 11:17
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="pl">
<head>
    <title>Instant Training Plan - personal</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, inital-scale=1, shrink-to-fit=no">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Lato:wght@300;400;700;900&family=Oswald:wght@200;300;400;500;700&display=swap"
          rel="stylesheet">
    <link rel="stylesheet" href="./resources/style.css">
</head>
<body>
<!-- navigation -->

<jsp:include page="fragment/navbar.jsp"/>

<!--Welcome info -->
<section>
    <div class="container">
        <div class="col-sm-2 col-md-8 col-md-offset-4">
            <form class="form-signin" method="post" action="personal">
                <h2 class="form-signin-heading">Podaj informacje do planu</h2>
                <label>Podaj cel treningu</label>
                <select name="inputGoal" class="custom-select" required>
                    <option value="1">Zwiększenie masy ciała.</option>
                    <option value="2">Redukcja masy ciała.</option>
                    <option value="3">Zwiększenie wydolności.</option>
                    <option value="4" selected>Ogólny rozwój.</option>
                    <option value="5">Zwiększenie siły.</option>
                </select>
                <label>Jakie masz doświadczenie w ćwiczeniach fizycznych i treningu?</label>
                <select name="inputExp" class="custom-select" required>
                    <option value="1" selected>Nigdy nie trenowałem.</option>
                    <option value="2">Trenowałem lata temu.</option>
                    <option value="3">Trenuje ale nie do końca wiem, co robię.</option>
                    <option value="4">Trenuje i znam ogólne zasady.</option>
                    <option value="5">Swobodnie wykonuje większość ćwiczeń.</option>
                    <option value="6">Mam duże doświadczenie, zrobię wszystko.</option>
                </select>
                <label>Ile ważysz?</label>
                <input name="inputWeight" type="number" class="form-control" placeholder="70" min="40" max="150"
                       required/>
                <label>Ile razy chcesz trenować w ciągu tygodnia?</label>
                <select name="inputTraining" class="custom-select" required>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3" selected>3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                    <option value="6">6</option>
                </select>
                <h3>Podaj urazy, które trzeba wziąć pod uwagę przy generowaniu planu.</h3>
                <div class=" displey-b">
                    <div class="row">
                        <div class="col-10"><label>Kark/szyja/odcinek szyjny kręgosłupa</label></div>
                        <div class="col-2"><input name="inputNeck" type="checkbox" class="form-check-input" value="1"
                                                  /></div>
                    </div>
                    <div class="row">
                        <div class="col-10"><label>Barki/Stawy ramienne</label></div>
                        <div class="col-2"><input name="inputShoulder" type="checkbox" class="form-check-input"
                                                  value="1" /></div>
                    </div>
                    <div class="row">
                        <div class="col-10"><label>Łokcie/Stawy łokciowe</label></div>
                        <div class="col-2"><input name="inputElbow" type="checkbox" class="form-check-input" value="1"
                                                  /></div>
                    </div>
                    <div class="row">
                        <div class="col-10"><label>Biodra/Stawy bioderowe</label></div>
                        <div class="col-2"><input name="inputHips" type="checkbox" class="form-check-input" value="1"
                                                  /></div>
                    </div>
                    <div class="row">
                        <div class="col-10"><label>Kolana/Stawy kolanowe</label></div>
                        <div class="col-2"><input name="inputKnees" type="checkbox" class="form-check-input" value="1"
                                                  /></div>
                    </div>
                    <div class="row">
                        <div class="col-10"><label>Kostki/Stawy skokowe</label></div>
                        <div class="col-2"><input name="inputAnkles" type="checkbox" class="form-check-input" value="1"
                                                  /></div>
                    </div>
                    <h3>Do jakich akcesoriów treningowych masz dostęp? Zaznaczając siłownie uważam, że znajdziemy na
                        niej wszystki poniższe przyrządy.</h3>
                    <div class="row">
                        <div class="col-10"><label>Siłownia</label></div>
                        <div class="col-2"><input name="inputGym" type="checkbox" class="form-check-input" value="1"
                                                  /></div>
                    </div>
                    <div class="row">
                        <div class="col-10"><label>Będę ćwiczył bez przyrządów.</label></div>
                        <div class="col-2"><input name="inputBody" type="checkbox" class="form-check-input" value="1"
                                                  /></div>
                    </div>
                    <div class="row">
                        <div class="col-10"><label>Pasy typu TRX</label></div>
                        <div class="col-2"><input name="inputTrx" type="checkbox" class="form-check-input" value="1"
                                                  /></div>
                    </div>
                    <div class="row">
                        <div class="col-10"><label>Obciążniki kettlebells</label></div>
                        <div class="col-2"><input name="inputKettle" type="checkbox" class="form-check-input" value="1"
                                                  /></div>
                    </div>
                    <div class="row">
                        <div class="col-10"><label>Ciężarki/Hantle różnej wagi</label></div>
                        <div class="col-2"><input name="inputDumb" type="checkbox" class="form-check-input" value="1"
                                                  /></div>
                    </div>
                    <div class="row">
                        <div class="col-10"><label>Drążek do podciągania</label></div>
                        <div class="col-2"><input name="inputBar" type="checkbox" class="form-check-input" value="1"
                                                  /></div>
                    </div>
                    <div class="row">
                        <div class="col-10"><label>BOSU</label></div>
                        <div class="col-2"><input name="inputBosu" type="checkbox" class="form-check-input" value="1"
                                                  /></div>
                    </div>
                    <div class="row">
                        <div class="col-10"><label>Taśmy/Gumy do ćwiczeń</label></div>
                        <div class="col-2"><input name="inputGum" type="checkbox" class="form-check-input" value="1"
                                                  /></div>
                    </div>
                </div>

                <button class="btn btn-lg btn-primary" type="submit">Przejdź dalej</button>
            </form>
        </div>
    </div>
</section>
<!--footer-->

<jsp:include page="fragment/footer.jsp"/>

</body>
</html>