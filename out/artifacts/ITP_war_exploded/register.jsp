<%--
  Created by IntelliJ IDEA.
  User: Admin
  Date: 2021-04-30
  Time: 16:34
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="pl">
<head>
  <title>Instant trainingTab Plan - register</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, inital-scale=1, shrink-to-fit=no">
  <link rel="preconnect" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css2?family=Lato:wght@300;400;700;900&family=Oswald:wght@200;300;400;500;700&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="./resources/style.css">
</head>
<body>
<!-- navigation -->

<jsp:include page="fragment/navbar.jsp"/>

<!--Registration -->
<section>
  <div class="col text-center mb-5">
    <form class="col-sm-8 col-md-6 col-lg-4 mx-auto d-block" method="post" action="register">
      <h1 class="mb-3">Zarejestruj się</h1>
      <label class="sr-only mb-3">Nazwa użytkownika</label>
      <input name="inputName" type="text" class="form-control mb-3"
             placeholder="Nazwa użytkownika">
      <label class="sr-only mb-3 ">Adres email</label>
      <input name="inputEmail" type="email" class="form-control mb-3" placeholder="Adres email"
             required="" autofocus="">
      <label class="sr-only mb-3">Hasło</label>
      <input name="inputPassword" type="password" class="form-control" placeholder="Hasło"
             required="">
      <div class="checkbox mb-3 mt-3">
      </div>
      <button class="btn btn-lg btn-block" type="submit">Zarejestruj się</button>
      <p class="mt-5 mb-3">© InstanttrainingTabPlan 2021</p>
    </form>
  </div>
</section>
<!--footer-->

<jsp:include page="fragment/footer.jsp"/>

</body>
</html>