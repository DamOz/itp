<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<head>
    <title>Instant trainingTab Plan - home page</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, inital-scale=1, shrink-to-fit=no">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Lato:wght@300;400;700;900&family=Oswald:wght@200;300;400;500;700&display=swap"
          rel="stylesheet">
    <link rel="stylesheet" href="./resources/style.css">
</head>
<body>
<!-- navigation -->

<jsp:include page="fragment/navbar.jsp"/>

<section>
    <div class="container">
        <div class="col-10 mx-auto text-center">
            <c:set var="training" value="${requestScope.training}"></c:set>
            <c:set var="user" value="${requestScope.user}"></c:set>
            <div class="text-uppercase">
                <h1>Twój plan <p><c:out value="${user.username}"></c:out></p></h1>
                <p>1) <c:out value="${training.e1}"></c:out> <c:out value="${training.s1}"></c:out> serie <c:out
                        value="${training.r1}"> </c:out> powtórzeń</p>
                <p>2) <c:out value="${training.e2}"></c:out> <c:out value="${training.s2}"></c:out> serie <c:out
                        value="${training.r2}"> </c:out> powtórzeń</p>
                <p>3) <c:out value="${training.e3}"></c:out> <c:out value="${training.s3}"></c:out> serie <c:out
                        value="${training.r3}"> </c:out> powtórzeń</p>
                <p>4) <c:out value="${training.e4}"></c:out> <c:out value="${training.s4}"></c:out> serie <c:out
                        value="${training.r4}"> </c:out> powtórzeń</p>
                <c:if test="${training.e5 ne null}">

                    <p>5) <c:out value="${training.e5}"></c:out> <c:out value="${training.s5}"></c:out> serie <c:out
                            value="${training.r5}"> </c:out> powtórzeń</p>

                </c:if>
                <c:if test="${training.e6 ne null}">

                    <p>6) <c:out value="${training.e6}"></c:out> <c:out value="${training.s6}"></c:out> serie <c:out
                            value="${training.r6}"></c:out> powtórzeń</p>

                </c:if>
                <c:if test="${training.e7 ne null}">

                    <p>7) <c:out value="${training.e7}"></c:out> <c:out value="${training.s7}"> </c:out> serie <c:out
                            value="${training.r7}"></c:out> powtórzeń</p>

                </c:if>
                <c:if test="${training.e8 ne null}">

                    <p>8) <c:out value="${training.e8}"></c:out><c:out value="${training.s8}"></c:out> serie <c:out
                            value="${training.r8}"></c:out> powtórzeń</p>

                </c:if>
                <c:if test="${training.e9 ne null}">

                    <p>9) <c:out value="${training.e9}"></c:out> <c:out value="${training.s9}"></c:out> serie <c:out
                            value="${training.r9}"></c:out> powtórzeń</p>

                </c:if>
                <c:if test="${training.e10 ne null}">

                    <p>10) <c:out value="${training.e10}"></c:out> <c:out value="${training.s10}"></c:out> serie <c:out
                            value="${training.r10}"></c:out> powtórzeń</p>

                </c:if>
                <c:if test="${training.e11 ne null}">

                    <p>11) <c:out value="${training.e11}"></c:out> <c:out value="${training.s11}"></c:out> serie <c:out
                            value="${training.r11}"></c:out> powtórzeń</p>

                </c:if>
                <c:if test="${training.e12 ne null}">

                    <p>12) <c:out value="${training.e12}"></c:out> <c:out value="${training.s12}"></c:out> serie <c:out
                            value="${training.r12}"></c:out> powtórzeń</p>

                </c:if>
                <c:if test="${training.e13 ne null}">

                    <p>13) <c:out value="${training.e13}"></c:out> <c:out value="${training.s13}"></c:out> serie <c:out
                            value="${training.r13}"></c:out> powtórzeń</p>

                </c:if>
                <c:if test="${training.e14 ne null}">

                    <p>14) <c:out value="${training.e14}"></c:out> <c:out value="${training.s14}"></c:out> serie <c:out
                            value="${training.r14}"></c:out> powtórzeń</p>

                </c:if>
                <c:if test="${training.e15 ne null}">

                    <p>15) <c:out value="${training.e15}"></c:out> <c:out value="${training.s15}"></c:out> serie <c:out
                            value="${training.r15}"></c:out> powtórzeń</p>

                </c:if>
                <c:if test="${training.e16 ne null}">

                    <p>16) <c:out value="${training.e16}"></c:out> <c:out value="${training.s16}"></c:out> serie <c:out
                            value="${training.r16}"></c:out> powtórzeń</p>

                </c:if>
                <c:if test="${training.e17 ne null}">

                    <p>17) <c:out value="${training.e17}"></c:out> <c:out value="${training.s17}"></c:out> serie <c:out
                            value="${training.r17}"></c:out> powtórzeń</p>

                </c:if>
                <c:if test="${training.e18 ne null}">

                    <p>18) <c:out value="${training.e18}"></c:out> <c:out value="${training.s18}"></c:out> serie <c:out
                            value="${training.r18}"></c:out> powtórzeń</p>

                </c:if>
                <c:if test="${training.e19 ne null}">

                    <p>19) <c:out value="${training.e19}"></c:out> <c:out value="${training.s19}"></c:out> serie <c:out
                            value="${training.r19}"></c:out> powtórzeń</p>

                </c:if>
                <c:if test="${training.e20 ne null}">

                    <p>20) <c:out value="${training.e20}"></c:out> <c:out value="${training.s20}"></c:out> serie <c:out
                            value="${training.r20}"></c:out> powtórzeń</p>

                </c:if>


            </div>
        </div>
    </div>
</section>

<!--footer-->

<jsp:include page="fragment/footer.jsp"/>

</body>
</html>
