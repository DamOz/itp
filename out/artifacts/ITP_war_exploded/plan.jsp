<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="pl">
<head>
    <title>Instant Training Plan - personal</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, inital-scale=1, shrink-to-fit=no">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Lato:wght@300;400;700;900&family=Oswald:wght@200;300;400;500;700&display=swap"
          rel="stylesheet">
    <link rel="stylesheet" href="./resources/style.css">
</head>
<body>
<!-- navigation -->

<jsp:include page="fragment/navbar.jsp"/>
<!-- full plan -->
<section>
    <div class="container">
        <div class="col-10 mx-auto text-center">
            <c:set var="plan" value="${requestScope.plan}"></c:set>
            <c:set var="user" value="${requestScope.user}"></c:set>
            <c:set var="trainingTab" value="${requestScope.trainingTab}"></c:set>
            <div class="text-uppercase">
                <h1>Twój plan <p><c:out value="${user.username}"></c:out></p></h1>
                <c:forEach var="trainingTab" items="${requestScope.trainingTab}">
                    <p>1) <c:out value="${trainingTab.e1}"></c:out> <c:out value="${trainingTab.s1}"></c:out> serie <c:out
                            value="${trainingTab.r1}"> </c:out> powtórzeń</p>
                    <p>2) <c:out value="${trainingTab.e2}"></c:out> <c:out value="${trainingTab.s2}"></c:out> serie <c:out
                            value="${trainingTab.r2}"> </c:out> powtórzeń</p>
                    <p>3) <c:out value="${trainingTab.e3}"></c:out> <c:out value="${trainingTab.s3}"></c:out> serie <c:out
                            value="${trainingTab.r3}"> </c:out> powtórzeń</p>
                    <p>4) <c:out value="${trainingTab.e4}"></c:out> <c:out value="${trainingTab.s4}"></c:out> serie <c:out
                            value="${trainingTab.r4}"> </c:out> powtórzeń</p>
                    <c:if test="${trainingTab.e5 ne null}">

                        <p>5) <c:out value="${trainingTab.e5}"></c:out> <c:out value="${trainingTab.s5}"></c:out> serie <c:out
                                value="${trainingTab.r5}"> </c:out> powtórzeń</p>

                    </c:if>
                    <c:if test="${trainingTab.e6 ne null}">

                        <p>6) <c:out value="${trainingTab.e6}"></c:out> <c:out value="${trainingTab.s6}"></c:out> serie <c:out
                                value="${trainingTab.r6}"></c:out> powtórzeń</p>

                    </c:if>
                    <c:if test="${trainingTab.e7 ne null}">

                        <p>7) <c:out value="${trainingTab.e7}"></c:out> <c:out value="${trainingTab.s7}"> </c:out> serie <c:out
                                value="${trainingTab.r7}"></c:out> powtórzeń</p>

                    </c:if>
                    <c:if test="${trainingTab.e8 ne null}">

                        <p>8) <c:out value="${trainingTab.e8}"></c:out><c:out value="${trainingTab.s8}"></c:out> serie <c:out
                                value="${trainingTab.r8}"></c:out> powtórzeń</p>

                    </c:if>
                    <c:if test="${trainingTab.e9 ne null}">

                        <p>9) <c:out value="${trainingTab.e9}"></c:out> <c:out value="${trainingTab.s9}"></c:out> serie <c:out
                                value="${trainingTab.r9}"></c:out> powtórzeń</p>

                    </c:if>
                    <c:if test="${trainingTab.e10 ne null}">

                        <p>10) <c:out value="${trainingTab.e10}"></c:out> <c:out value="${trainingTab.s10}"></c:out> serie <c:out
                                value="${trainingTab.r10}"></c:out> powtórzeń</p>

                    </c:if>
                    <c:if test="${trainingTab.e11 ne null}">

                        <p>11) <c:out value="${trainingTab.e11}"></c:out> <c:out value="${trainingTab.s11}"></c:out> serie <c:out
                                value="${trainingTab.r11}"></c:out> powtórzeń</p>

                    </c:if>
                    <c:if test="${trainingTab.e12 ne null}">

                        <p>12) <c:out value="${trainingTab.e12}"></c:out> <c:out value="${trainingTab.s12}"></c:out> serie <c:out
                                value="${trainingTab.r12}"></c:out> powtórzeń</p>

                    </c:if>
                    <c:if test="${trainingTab.e13 ne null}">

                        <p>13) <c:out value="${trainingTab.e13}"></c:out> <c:out value="${trainingTab.s13}"></c:out> serie <c:out
                                value="${trainingTab.r13}"></c:out> powtórzeń</p>

                    </c:if>
                    <c:if test="${trainingTab.e14 ne null}">

                        <p>14) <c:out value="${trainingTab.e14}"></c:out> <c:out value="${trainingTab.s14}"></c:out> serie <c:out
                                value="${trainingTab.r14}"></c:out> powtórzeń</p>

                    </c:if>
                    <c:if test="${trainingTab.e15 ne null}">

                        <p>15) <c:out value="${trainingTab.e15}"></c:out> <c:out value="${trainingTab.s15}"></c:out> serie <c:out
                                value="${trainingTab.r15}"></c:out> powtórzeń</p>

                    </c:if>
                    <c:if test="${trainingTab.e16 ne null}">

                        <p>16) <c:out value="${trainingTab.e16}"></c:out> <c:out value="${trainingTab.s16}"></c:out> serie <c:out
                                value="${trainingTab.r16}"></c:out> powtórzeń</p>

                    </c:if>
                    <c:if test="${trainingTab.e17 ne null}">

                        <p>17) <c:out value="${trainingTab.e17}"></c:out> <c:out value="${trainingTab.s17}"></c:out> serie <c:out
                                value="${trainingTab.r17}"></c:out> powtórzeń</p>

                    </c:if>
                    <c:if test="${trainingTab.e18 ne null}">

                        <p>18) <c:out value="${trainingTab.e18}"></c:out> <c:out value="${trainingTab.s18}"></c:out> serie <c:out
                                value="${trainingTab.r18}"></c:out> powtórzeń</p>

                    </c:if>
                    <c:if test="${trainingTab.e19 ne null}">

                        <p>19) <c:out value="${trainingTab.e19}"></c:out> <c:out value="${trainingTab.s19}"></c:out> serie <c:out
                                value="${trainingTab.r19}"></c:out> powtórzeń</p>

                    </c:if>
                    <c:if test="${trainingTab.e20 ne null}">

                        <p>20) <c:out value="${trainingTab.e20}"></c:out> <c:out value="${trainingTab.s20}"></c:out> serie <c:out
                                value="${trainingTab.r20}"></c:out> powtórzeń</p>

                    </c:if>
                </c:forEach>
            </div>
        </div>
    </div>
</section>

<!--footer-->

<jsp:include page="fragment/footer.jsp"/>

</body>
</html>