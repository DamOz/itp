package pl.damianozga.itp.submodels.generalDevelopment;


import pl.damianozga.itp.checker.CheckForRepetition;
import pl.damianozga.itp.chooser.ExerciseChooser;
import pl.damianozga.itp.chooser.ExerciseChooser;
import pl.damianozga.itp.model.Exercise;
import pl.damianozga.itp.model.Personal;
import pl.damianozga.itp.model.Training;
import pl.damianozga.itp.model.User;

import java.util.ArrayList;

public class GeneralDevelopmentOneTraining extends Training {

    private int rep1 = 8;
    private int rep2 = 10;
    private int rep3 = 20;
    private int rep4 = 26;
    private int set1 = 3;
    private int set2 = 4;


    public Training createTraining(User user, ArrayList<Exercise> exercisesList, Personal personal) {
        Training training = new GeneralDevelopmentOneTraining();
        training.setUser(user);
        ExerciseChooser c = new ExerciseChooser();
        training.setE1(c.getExerciseCzworoglowy(exercisesList, personal).getName()); training.setS1(set1); training.setR1(rep1);
        training.setE2(c.getExerciseDwuglowy(exercisesList, personal).getName()); training.setS2(set1); training.setR2(rep1);
        training.setE3(c.getExerciseLydka(exercisesList, personal).getName()); training.setS3(set1); training.setR3(rep3);
        training.setE4(c.getExerciseNajszerszyDol(exercisesList, personal).getName()); training.setS4(set2);training.setR4(rep1);
        training.setE5(c.getExerciseKlatkaDol(exercisesList, personal).getName()); training.setS5(set2); training.setR5(rep1);
        training.setE6(c.getExerciseBarkiSrodek(exercisesList, personal).getName()); training.setS6(set2); training.setR6(rep1);
        training.setE7(c.getExerciseBiceps(exercisesList, personal).getName()); training.setS7(set2);training.setR7(rep2);
        training.setE8(c.getExerciseTriceps(exercisesList, personal).getName()); training.setS8(set2);training.setR8(rep2);
        training.setE9(c.getExerciseBrzuchGora(exercisesList, personal).getName()); training.setS9(set1); training.setR9(rep4);
        training.setE10(c.getExerciseBrzuchDol(exercisesList, personal).getName()); training.setS10(set1);training.setR10(rep4);
        return training;
    }
}
