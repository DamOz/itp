package pl.damianozga.itp.service;

import pl.damianozga.itp.dao.DAOFactory;
import pl.damianozga.itp.dao.TrainingDAO;
import pl.damianozga.itp.model.Exercise;
import pl.damianozga.itp.model.Training;
import pl.damianozga.itp.model.User;

import java.util.List;

public class TrainingService {

    public void addPreperedTraining(Training preperedTraining){
        Training resultTreining = createTrainingObject(
                preperedTraining.getUser(),
                preperedTraining.getE1(),
                preperedTraining.getE2(),
                preperedTraining.getE3(),
                preperedTraining.getE4(),
                preperedTraining.getE5(),
                preperedTraining.getE6(),
                preperedTraining.getE7(),
                preperedTraining.getE8(),
                preperedTraining.getE9(),
                preperedTraining.getE10(),
                preperedTraining.getE11(),
                preperedTraining.getE12(),
                preperedTraining.getE13(),
                preperedTraining.getE14(),
                preperedTraining.getE15(),
                preperedTraining.getE16(),
                preperedTraining.getE17(),
                preperedTraining.getE18(),
                preperedTraining.getE19(),
                preperedTraining.getE20(),
                preperedTraining.getR1(),
                preperedTraining.getR2(),
                preperedTraining.getR3(),
                preperedTraining.getR4(),
                preperedTraining.getR5(),
                preperedTraining.getR6(),
                preperedTraining.getR7(),
                preperedTraining.getR8(),
                preperedTraining.getR9(),
                preperedTraining.getR10(),
                preperedTraining.getR11(),
                preperedTraining.getR12(),
                preperedTraining.getR13(),
                preperedTraining.getR14(),
                preperedTraining.getR15(),
                preperedTraining.getR16(),
                preperedTraining.getR17(),
                preperedTraining.getR18(),
                preperedTraining.getR19(),
                preperedTraining.getR20(),
                preperedTraining.getS1(),
                preperedTraining.getS2(),
                preperedTraining.getS3(),
                preperedTraining.getS4(),
                preperedTraining.getS5(),
                preperedTraining.getS6(),
                preperedTraining.getS7(),
                preperedTraining.getS8(),
                preperedTraining.getS9(),
                preperedTraining.getS10(),
                preperedTraining.getS11(),
                preperedTraining.getS12(),
                preperedTraining.getS13(),
                preperedTraining.getS14(),
                preperedTraining.getS15(),
                preperedTraining.getS16(),
                preperedTraining.getS17(),
                preperedTraining.getS18(),
                preperedTraining.getS19(),
                preperedTraining.getS20());

        DAOFactory factory = DAOFactory.getDAOFactory();
        TrainingDAO trainingDao = factory.getTrainingDAO();
        trainingDao.create(resultTreining);
    }

    public void addTraining(User user, String e1, String e2, String e3, String e4, String e5, String e6, String e7,
                       String e8, String e9, String e10, String e11, String e12, String e13, String e14, String e15,
                       String e16, String e17, String e18, String e19, String e20, int r1, int r2, int r3,int r4,
                            int r5, int r6, int r7, int r8, int r9, int r10, int r11, int r12, int r13,int r14,
                            int r15, int r16, int r17, int r18, int r19, int r20, int s1, int s2, int s3, int s4,
                            int s5, int s6, int s7, int s8, int s9, int s10, int s11, int s12, int s13,int s14,
                            int s15, int s16, int s17, int s18, int s19, int s20) {
        Training training = createTrainingObject(user, e1, e2, e3, e4, e5, e6, e7, e8, e9, e10, e11, e12, e13, e14,
                e15, e16, e17, e18, e19, e20, r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14,
                r15, r16, r17, r18, r19, r20, s1, s2, s3, s4, s5, s6, s7, s8, s9, s10, s11, s12, s13, s14,
                s15, s16, s17, s18, s19, s20);
        DAOFactory factory = DAOFactory.getDAOFactory();
        TrainingDAO trainingDao = factory.getTrainingDAO();
        trainingDao.create(training);
    }

    private Training createTrainingObject(User user, String e1, String e2, String e3, String e4, String e5, String e6,
                                          String e7, String e8, String e9, String e10, String e11, String e12,
                                          String e13, String e14, String e15, String e16, String e17, String e18,
                                          String e19, String e20, int r1, int r2, int r3,int r4,
                                          int r5, int r6, int r7, int r8, int r9, int r10, int r11,
                                          int r12, int r13,int r14, int r15, int r16, int r17, int r18,
                                          int r19, int r20, int s1, int s2, int s3, int s4,
                                          int s5, int s6, int s7, int s8, int s9, int s10, int s11,
                                          int s12, int s13,int s14, int s15, int s16, int s17, int s18,
                                          int s19, int s20){
        Training training = new Training();
        User userCopy = new User(user);
        training.setUser(userCopy);
        training.setE1(e1);
        training.setE2(e2);
        training.setE3(e3);
        training.setE4(e4);
        training.setE5(e5);
        training.setE6(e6);
        training.setE7(e7);
        training.setE8(e8);
        training.setE9(e9);
        training.setE10(e10);
        training.setE11(e11);
        training.setE12(e12);
        training.setE13(e13);
        training.setE14(e14);
        training.setE15(e15);
        training.setE16(e16);
        training.setE17(e17);
        training.setE18(e18);
        training.setE19(e19);
        training.setE20(e20);
        training.setR1(r1);
        training.setR2(r2);
        training.setR3(r3);
        training.setR4(r4);
        training.setR5(r5);
        training.setR6(r6);
        training.setR7(r7);
        training.setR8(r8);
        training.setR9(r9);
        training.setR10(r10);
        training.setR11(r11);
        training.setR12(r12);
        training.setR13(r13);
        training.setR14(r14);
        training.setR15(r15);
        training.setR16(r16);
        training.setR17(r17);
        training.setR18(r18);
        training.setR19(r19);
        training.setR20(r20);
        training.setS1(s1);
        training.setS2(s2);
        training.setS3(s3);
        training.setS4(s4);
        training.setS5(s5);
        training.setS6(s6);
        training.setS7(s7);
        training.setS8(s8);
        training.setS9(s9);
        training.setS10(s10);
        training.setS11(s11);
        training.setS12(s12);
        training.setS13(s13);
        training.setS14(s14);
        training.setS15(s15);
        training.setS16(s16);
        training.setS17(s17);
        training.setS18(s18);
        training.setS19(s19);
        training.setS20(s20);
        return training;
    }

    public Training getTrainingById(long user_id){
        DAOFactory factory = DAOFactory.getDAOFactory();
        TrainingDAO trainingDao = factory.getTrainingDAO();
        Training training = trainingDao.getTrainingById(user_id);
        return training;
    }

    public List<Training> getAllTrainingsByUserId(long user_id){
        DAOFactory factory = DAOFactory.getDAOFactory();
        TrainingDAO trainingDao = factory.getTrainingDAO();
        List<Training> trainings = trainingDao.getAllTrainigsByUserId(user_id);
        return  trainings;
    }
}
