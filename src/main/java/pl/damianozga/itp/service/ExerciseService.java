package pl.damianozga.itp.service;

import pl.damianozga.itp.dao.DAOFactory;
import pl.damianozga.itp.dao.ExerciseDAO;
import pl.damianozga.itp.model.Exercise;

import java.util.List;

public class ExerciseService {

    public void addExercise(String name, int bodypart, int dif, int gym, int item, int injury) {
        Exercise exercise = new Exercise();
        exercise.setName(name);
        exercise.setBodypart(bodypart);
        exercise.setDif(dif);
        exercise.setGym(gym);
        exercise.setItem(item);
        exercise.setInjury(injury);
        DAOFactory factory = DAOFactory.getDAOFactory();
        ExerciseDAO exerciseDao = factory.getExerciseDAO();
        exerciseDao.create(exercise);
    }

    public Exercise getExerciseById(long id) {
        DAOFactory factory = DAOFactory.getDAOFactory();
        ExerciseDAO exerciseDao = factory.getExerciseDAO();
        Exercise exercise = exerciseDao.read(id);
        return exercise;
    }

    public List<Exercise> getAllExercises() {
        DAOFactory factory = DAOFactory.getDAOFactory();
        ExerciseDAO exerciseDao = factory.getExerciseDAO();
        List<Exercise> exercises = exerciseDao.getAll();
        return exercises;
    }
}
