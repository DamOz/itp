package pl.damianozga.itp.service;

import pl.damianozga.itp.dao.DAOFactory;
import pl.damianozga.itp.dao.PlanDAO;
import pl.damianozga.itp.dao.TrainingDAO;
import pl.damianozga.itp.model.Plan;
import pl.damianozga.itp.model.Training;
import pl.damianozga.itp.model.User;

public class PlanService {

    public void addPlan(int breaks, int cardio, Training t1, Training t2, Training t3, Training t4,
                        Training t5, Training t6, User user){
        Plan plan = createPlanObject(breaks, cardio, t1, t2, t3, t4, t5, t6, user);
        DAOFactory factory = DAOFactory.getDAOFactory();
        PlanDAO planDao = factory.getPlanDAO();
        planDao.create(plan);
    }

    private Plan createPlanObject(int breaks, int cardio, Training t1, Training t2, Training t3, Training t4,
                                  Training t5, Training t6, User user){
        Plan plan = new Plan();
        plan.setBreaks(breaks);
        plan.setCardio(cardio);
        Training trainingCopy1 = new Training(t1);
        plan.setT1(trainingCopy1);
        Training trainingCopy2 = new Training(t2);
        plan.setT2(trainingCopy2);
        Training trainingCopy3 = new Training(t3);
        plan.setT3(trainingCopy3);
        Training trainingCopy4 = new Training(t4);
        plan.setT4(trainingCopy4);
        Training trainingCopy5 = new Training(t5);
        plan.setT5(trainingCopy5);
        Training trainingCopy6 = new Training(t6);
        plan.setT6(trainingCopy6);
        User copyUser = new User(user);
        plan.setUser(copyUser);
        return plan;
    }

    public Plan getPlanByUserId(long user_id){
        DAOFactory factory = DAOFactory.getDAOFactory();
        PlanDAO planDao = factory.getPlanDAO();
        Plan plan = planDao.getPlanByUserId(user_id);
        return plan;
    }

}
