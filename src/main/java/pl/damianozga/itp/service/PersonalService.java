package pl.damianozga.itp.service;

import pl.damianozga.itp.dao.DAOFactory;
import pl.damianozga.itp.dao.PersonalDAO;
import pl.damianozga.itp.model.Personal;

public class PersonalService {

    public void addPersonal( long id, int goal, int exp, int weight, int training, int neck, int shoulder, int elbow,
                             int hips, int knees, int ankles, int gym, int body, int trx, int kettle,
                             int dumb, int bar, int bosu, int gum) {
        Personal personal = new Personal();
        personal.setId(id);
        personal.setGoal(goal);
        personal.setExp(exp);
        personal.setWeight(weight);
        personal.setTraining(training);
        personal.setNeck(neck);
        personal.setShoulder(shoulder);
        personal.setElbow(elbow);
        personal.setHips(hips);
        personal.setKnees(knees);
        personal.setAnkles(ankles);
        personal.setGym(gym);
        personal.setBody(body);
        personal.setTrx(trx);
        personal.setKettle(kettle);
        personal.setDumb(dumb);
        personal.setBar(bar);
        personal.setBosu(bosu);
        personal.setGum(gum);
        DAOFactory factory = DAOFactory.getDAOFactory();
        PersonalDAO personalDao = factory.getPersonalDAO();
        personalDao.create(personal);
    }

    public Personal getPersonalById(long id){
        DAOFactory factory = DAOFactory.getDAOFactory();
        PersonalDAO personalDao = factory.getPersonalDAO();
        Personal personal = personalDao.read(id);
        return personal;
    }
}
