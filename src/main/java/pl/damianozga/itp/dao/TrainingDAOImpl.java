package pl.damianozga.itp.dao;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import pl.damianozga.itp.model.Training;
import pl.damianozga.itp.model.User;
import pl.damianozga.itp.util.ConnectionProvider;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TrainingDAOImpl implements TrainingDAO {

    private static final String CREAT_TRAINING =
            "INSERT INTO training (user_id, e1, e2, e3, e4, e5, e6, e7, e8, e9, e10, e11, e12, e13, e14, e15, e16, e17, e18, e19, e20, r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15, r16, r17, r18, r19, r20, s1, s2, s3, s4, s5, s6, s7, s8, s9, s10, s11, s12, s13, s14, s15, s16, s17, s18, s19, s20) VALUES (:user_id, :e1, :e2, :e3, :e4, :e5, :e6, :e7, :e8, :e9, :e10, :e11, :e12, :e13, :e14, :e15, :e16, :e17, :e18, :e19, :e20, :r1, :r2, :r3, :r4, :r5, :r6, :r7, :r8, :r9, :r10, :r11, :r12, :r13, :r14, :r15, :r16, :r17, :r18, :r19, :r20, :s1, :s2, :s3, :s4, :s5, :s6, :s7, :s8, :s9, :s10, :s11, :s12, :s13, :s14, :s15, :s16, :s17, :s18, :s19, :s20);";

    private static final String READ_TRAINING =
            "SELECT id, training.user_id, e1, e2, e3, e4, e5, e6, e7, e8, e9, e10, e11, e12, e13, e14, e15, e16, e17, e18, e19, e20, r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15, r16, r17, r18, r19, r20, s1, s2, s3, s4, s5, s6, s7, s8, s9, s10, s11, s12, s13, s14, s15, s16, s17, s18, s19, s20 FROM training LEFT JOIN user ON training.user_id =user.user_id WHERE training.user_id=:training.user_id;";

    private static final String READ_TRAINING_BY_USER_ID =
            "SELECT id, training.user_id, e1, e2, e3, e4, e5, e6, e7, e8, e9, e10, e11, e12, e13, e14, e15, e16, e17, e18, e19, e20, r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15, r16, r17, r18, r19, r20, s1, s2, s3, s4, s5, s6, s7, s8, s9, s10, s11, s12, s13, s14, s15, s16, s17, s18, s19, s20 FROM training LEFT JOIN user ON training.user_id =user.user_id WHERE training.user_id=:user_id;";

    private NamedParameterJdbcTemplate template;

    public TrainingDAOImpl() {
        template = new NamedParameterJdbcTemplate(ConnectionProvider.getDataSource());
    }

    @Override
    public Training getTrainingById(long user_id) {
    Training resultTraining ;
    SqlParameterSource parameterSource = new MapSqlParameterSource("training.user_id", user_id);
    resultTraining = template.queryForObject(READ_TRAINING, parameterSource, new TrainingRowMapper());
        return resultTraining;
    }

    @Override
    public List<Training> getTrainingByUserId(long userId) {
        return null;
    }

    @Override
    public Training create(Training training) {
        Training resultTrainig = new Training(training);
        KeyHolder keyHolder = new GeneratedKeyHolder();
        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("user_id", resultTrainig.getUser().getId());
        paramMap.put("e1",resultTrainig.getE1());
        paramMap.put("e2",resultTrainig.getE2());
        paramMap.put("e3",resultTrainig.getE3());
        paramMap.put("e4",resultTrainig.getE4());
        paramMap.put("e5",resultTrainig.getE5());
        paramMap.put("e6",resultTrainig.getE6());
        paramMap.put("e7",resultTrainig.getE7());
        paramMap.put("e8",resultTrainig.getE8());
        paramMap.put("e9",resultTrainig.getE9());
        paramMap.put("e10",resultTrainig.getE10());
        paramMap.put("e11",resultTrainig.getE11());
        paramMap.put("e12",resultTrainig.getE12());
        paramMap.put("e13",resultTrainig.getE13());
        paramMap.put("e14",resultTrainig.getE14());
        paramMap.put("e15",resultTrainig.getE15());
        paramMap.put("e16",resultTrainig.getE16());
        paramMap.put("e17",resultTrainig.getE17());
        paramMap.put("e18",resultTrainig.getE18());
        paramMap.put("e19",resultTrainig.getE19());
        paramMap.put("e20",resultTrainig.getE20());
        paramMap.put("r1",resultTrainig.getR1());
        paramMap.put("r2",resultTrainig.getR2());
        paramMap.put("r3",resultTrainig.getR3());
        paramMap.put("r4",resultTrainig.getR4());
        paramMap.put("r5",resultTrainig.getR5());
        paramMap.put("r6",resultTrainig.getR6());
        paramMap.put("r7",resultTrainig.getR7());
        paramMap.put("r8",resultTrainig.getR8());
        paramMap.put("r9",resultTrainig.getR9());
        paramMap.put("r10",resultTrainig.getR10());
        paramMap.put("r11",resultTrainig.getR11());
        paramMap.put("r12",resultTrainig.getR12());
        paramMap.put("r13",resultTrainig.getR13());
        paramMap.put("r14",resultTrainig.getR14());
        paramMap.put("r15",resultTrainig.getR15());
        paramMap.put("r16",resultTrainig.getR16());
        paramMap.put("r17",resultTrainig.getR17());
        paramMap.put("r18",resultTrainig.getR18());
        paramMap.put("r19",resultTrainig.getR19());
        paramMap.put("r20",resultTrainig.getR20());
        paramMap.put("s1",resultTrainig.getS1());
        paramMap.put("s2",resultTrainig.getS2());
        paramMap.put("s3",resultTrainig.getS3());
        paramMap.put("s4",resultTrainig.getS4());
        paramMap.put("s5",resultTrainig.getS5());
        paramMap.put("s6",resultTrainig.getS6());
        paramMap.put("s7",resultTrainig.getS7());
        paramMap.put("s8",resultTrainig.getS8());
        paramMap.put("s9",resultTrainig.getS9());
        paramMap.put("s10",resultTrainig.getS10());
        paramMap.put("s11",resultTrainig.getS11());
        paramMap.put("s12",resultTrainig.getS12());
        paramMap.put("s13",resultTrainig.getS13());
        paramMap.put("s14",resultTrainig.getS14());
        paramMap.put("s15",resultTrainig.getS15());
        paramMap.put("s16",resultTrainig.getS16());
        paramMap.put("s17",resultTrainig.getS17());
        paramMap.put("s18",resultTrainig.getS18());
        paramMap.put("s19",resultTrainig.getS19());
        paramMap.put("s20",resultTrainig.getS20());
        SqlParameterSource parameterSource = new MapSqlParameterSource(paramMap);
        int update = template.update(CREAT_TRAINING, parameterSource, keyHolder);
        if(update > 0){
            resultTrainig.setId(keyHolder.getKey().longValue());
        }
        return resultTrainig;
    }

    @Override
    public Training read(Long user_id) {
        Training resultTraining = null;
        SqlParameterSource parameterSource = new MapSqlParameterSource("user_id", user_id);
        resultTraining = template.queryForObject(READ_TRAINING, parameterSource, new TrainingRowMapper());
        return resultTraining;
    }

    @Override
    public boolean update(Training updateObject) {
        return false;
    }

    @Override
    public boolean delete(Long key) {
        return false;
    }

    @Override
    public List<Training> getAll() {
        return null;
    }

    @Override
    public List<Training> getAllTrainigsByUserId(long user_id) {
        List<Training> resultTrainings = null;
        SqlParameterSource parameterSource = new MapSqlParameterSource("user_id", user_id);
        resultTrainings = template.query(READ_TRAINING_BY_USER_ID, parameterSource, new TrainingRowMapper());
        return resultTrainings;
    }

    private class TrainingRowMapper implements RowMapper<Training>{
        @Override
        public Training mapRow(ResultSet resultSet, int i) throws SQLException {
            Training training = new Training();
            training.setId(resultSet.getLong("id"));
            User user = new User();
            training.setUser(user);
            user.setId(resultSet.getLong("user_id"));
            training.setE1(resultSet.getString("e1"));
            training.setE2(resultSet.getString("e2"));
            training.setE3(resultSet.getString("e3"));
            training.setE4(resultSet.getString("e4"));
            training.setE5(resultSet.getString("e5"));
            training.setE6(resultSet.getString("e6"));
            training.setE7(resultSet.getString("e7"));
            training.setE8(resultSet.getString("e8"));
            training.setE9(resultSet.getString("e9"));
            training.setE10(resultSet.getString("e10"));
            training.setE11(resultSet.getString("e11"));
            training.setE12(resultSet.getString("e12"));
            training.setE13(resultSet.getString("e13"));
            training.setE14(resultSet.getString("e14"));
            training.setE15(resultSet.getString("e15"));
            training.setE16(resultSet.getString("e16"));
            training.setE17(resultSet.getString("e17"));
            training.setE18(resultSet.getString("e18"));
            training.setE19(resultSet.getString("e19"));
            training.setE20(resultSet.getString("e20"));
            training.setR1(resultSet.getInt("r1"));
            training.setR2(resultSet.getInt("r2"));
            training.setR3(resultSet.getInt("r3"));
            training.setR4(resultSet.getInt("r4"));
            training.setR5(resultSet.getInt("r5"));
            training.setR6(resultSet.getInt("r6"));
            training.setR7(resultSet.getInt("r7"));
            training.setR8(resultSet.getInt("r8"));
            training.setR9(resultSet.getInt("r9"));
            training.setR10(resultSet.getInt("r10"));
            training.setR11(resultSet.getInt("r11"));
            training.setR12(resultSet.getInt("r12"));
            training.setR13(resultSet.getInt("r13"));
            training.setR14(resultSet.getInt("r14"));
            training.setR15(resultSet.getInt("r15"));
            training.setR16(resultSet.getInt("r16"));
            training.setR17(resultSet.getInt("r17"));
            training.setR18(resultSet.getInt("r18"));
            training.setR19(resultSet.getInt("r19"));
            training.setR20(resultSet.getInt("r20"));
            training.setS1(resultSet.getInt("s1"));
            training.setS2(resultSet.getInt("s2"));
            training.setS3(resultSet.getInt("s3"));
            training.setS4(resultSet.getInt("s4"));
            training.setS5(resultSet.getInt("s5"));
            training.setS6(resultSet.getInt("s6"));
            training.setS7(resultSet.getInt("s7"));
            training.setS8(resultSet.getInt("s8"));
            training.setS9(resultSet.getInt("s9"));
            training.setS10(resultSet.getInt("s10"));
            training.setS11(resultSet.getInt("s11"));
            training.setS12(resultSet.getInt("s12"));
            training.setS13(resultSet.getInt("s13"));
            training.setS14(resultSet.getInt("s14"));
            training.setS15(resultSet.getInt("s15"));
            training.setS16(resultSet.getInt("s16"));
            training.setS17(resultSet.getInt("s17"));
            training.setS18(resultSet.getInt("s18"));
            training.setS19(resultSet.getInt("s19"));
            training.setS20(resultSet.getInt("s20"));
            return training;
        }
    }
}
