package pl.damianozga.itp.dao;

import pl.damianozga.itp.model.Training;

import java.util.List;

public interface TrainingDAO extends GenericDAO<Training, Long>{

    Training getTrainingById(long id);
    List<Training> getTrainingByUserId(long userId);
    List<Training> getAllTrainigsByUserId(long user_id);
}
