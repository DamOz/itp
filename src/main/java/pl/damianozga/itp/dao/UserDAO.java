package pl.damianozga.itp.dao;

import pl.damianozga.itp.model.User;

import java.util.List;

public interface UserDAO extends GenericDAO<User, Long> {

    List<User> getAll();
    User getUserById(long id);
    User getUserByUsername(String username);

}
