package pl.damianozga.itp.dao;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import pl.damianozga.itp.model.Personal;
import pl.damianozga.itp.util.ConnectionProvider;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class PersonalDAOImpl implements PersonalDAO {

    private static final String CREATE_PERSONAL =
            "INSERT INTO personal(id, goal, exp, weight, training, neck, shoulder, elbow, hips, knees, ankles, gym, body, trx, kettle, dumb, bar, bosu, gum) VALUES (:id, :goal, :exp, :weight, :training, :neck, :shoulder, :elbow, :hips, :knees, :ankles, :gym, :body, :trx, :kettle, :dumb, :bar, :bosu, :gum);";

    private static final String READ_PERSONAL =
            "SELECT id, goal, exp, weight, training, neck, shoulder, elbow, hips, knees, ankles, gym, body, trx, kettle, dumb, bar, bosu, gum FROM personal WHERE personal.id= :id;";


    private NamedParameterJdbcTemplate template;

    public PersonalDAOImpl() {
        template = new NamedParameterJdbcTemplate(ConnectionProvider.getDataSource());
    }

    // CRUD

    @Override
    public Personal getPersonalById(long id) {
        return null;
    }


    @Override
    public Personal create(Personal personal) {
        Personal resultPersonal = new Personal(personal);
        KeyHolder keyHolder = new GeneratedKeyHolder();
        SqlParameterSource sqlParameterSource = new BeanPropertySqlParameterSource(personal);
        int update = template.update(CREATE_PERSONAL, sqlParameterSource, keyHolder);
        if(update > 0 ){
            resultPersonal.setId(keyHolder.getKey().longValue());
        }
        return resultPersonal;
    }

    @Override
    public Personal read(Long primaryKey) {
        Personal resultPersonal;
        SqlParameterSource parameterSource = new MapSqlParameterSource("id", primaryKey);
        resultPersonal = template.queryForObject(READ_PERSONAL, parameterSource, new PersonalRowMapper());
        return resultPersonal;
    }

    @Override
    public boolean update(Personal updateObject) {
        return false;
    }

    @Override
    public boolean delete(Long key) {
        return false;
    }

    @Override
    public List<Personal> getAll() {
        return null;
    }

    private class PersonalRowMapper implements RowMapper<Personal>{
        @Override
        public Personal mapRow(ResultSet resultSet, int i) throws SQLException {
            Personal personal = new Personal();
            personal.setId(resultSet.getLong("id"));
            personal.setGoal(resultSet.getInt("goal"));
            personal.setExp(resultSet.getInt("exp"));
            personal.setWeight(resultSet.getInt("weight"));
            personal.setTraining(resultSet.getInt("training"));
            personal.setNeck(resultSet.getInt("neck"));
            personal.setShoulder(resultSet.getInt("shoulder"));
            personal.setElbow(resultSet.getInt("elbow"));
            personal.setHips(resultSet.getInt("hips"));
            personal.setKnees(resultSet.getInt("knees"));
            personal.setAnkles(resultSet.getInt("ankles"));
            personal.setGym(resultSet.getInt("gym"));
            personal.setBody(resultSet.getInt("body"));
            personal.setTrx(resultSet.getInt("trx"));
            personal.setKettle(resultSet.getInt("kettle"));
            personal.setDumb(resultSet.getInt("dumb"));
            personal.setBar(resultSet.getInt("bar"));
            personal.setBosu(resultSet.getInt("bosu"));
            personal.setGum(resultSet.getInt("gum"));
            return personal;
        }
    }
}
