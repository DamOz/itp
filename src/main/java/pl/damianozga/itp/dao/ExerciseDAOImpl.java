package pl.damianozga.itp.dao;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import pl.damianozga.itp.model.Exercise;
import pl.damianozga.itp.model.Personal;
import pl.damianozga.itp.util.ConnectionProvider;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class ExerciseDAOImpl implements ExerciseDAO {


    private static final String CREATE_EXERCISE =
            "INSERT INTO exercise ( name, bodypart, dif, gym, item, injury) VALUES (:name, :bodypart, :dif, :gym, :item, :injury);";
    private static final String READ_EXERCISE =
            "SELECT id, name, bodypart, dif, gym, item, injury FROM exercise WHERE exercise.id= :id;";
    private static final String READ_ALL_EXERCISE =
            "SELECT id, name, bodypart, dif, gym, item, injury FROM exercise;";

    private NamedParameterJdbcTemplate template;

    public ExerciseDAOImpl() {
        template = new NamedParameterJdbcTemplate(ConnectionProvider.getDataSource());
    }

    @Override
    public List<Exercise> getAll() {
        List<Exercise> exercises = template.query(READ_ALL_EXERCISE, new ExerciseRowMapper());
        return exercises;
    }

    @Override
    public Exercise getExerciseById(long id) {
        return null;
    }

    @Override
    public Exercise create(Exercise exercise) {
        Exercise resultExercise = new Exercise(exercise);
        KeyHolder keyHolder = new GeneratedKeyHolder();
        SqlParameterSource parameterSource = new BeanPropertySqlParameterSource(exercise);
        int update = template.update(CREATE_EXERCISE, parameterSource, keyHolder);
        if(update > 1) {
            resultExercise.setId(keyHolder.getKey().longValue());
        }
        return resultExercise;
    }

    @Override
    public Exercise read(Long id) {
        Exercise resultExercise = null;
        SqlParameterSource parameterSource = new MapSqlParameterSource("id", id);
        resultExercise = template.queryForObject(READ_EXERCISE, parameterSource, new ExerciseRowMapper());
        return resultExercise;
    }

    @Override
    public boolean update(Exercise updateObject) {
        return false;
    }

    @Override
    public boolean delete(Long key) {
        return false;
    }

    private class ExerciseRowMapper implements RowMapper<Exercise>{
        @Override
        public Exercise mapRow(ResultSet resultSet, int i) throws SQLException {
           Exercise exercise = new Exercise();
            exercise.setId(resultSet.getLong("id"));
            exercise.setName(resultSet.getString("name"));
            exercise.setBodypart(resultSet.getInt("bodypart"));
            exercise.setDif(resultSet.getInt("dif"));
            exercise.setGym(resultSet.getInt("gym"));
            exercise.setItem(resultSet.getInt("item"));
            exercise.setInjury(resultSet.getInt("injury"));
            return exercise;
        }
    }
}
