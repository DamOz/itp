package pl.damianozga.itp.dao;

import pl.damianozga.itp.model.Plan;

public interface PlanDAO extends GenericDAO<Plan, Long> {

    Plan getPlanByUserId(long userId);
}
