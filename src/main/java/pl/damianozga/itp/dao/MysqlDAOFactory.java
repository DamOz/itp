package pl.damianozga.itp.dao;

import javax.sql.DataSource;

public class MysqlDAOFactory extends DAOFactory {

    @Override
    public UserDAO getUserDAO() {
        return new UserDAOImpl();
    }

    @Override
    public PersonalDAO getPersonalDAO() {
        return new PersonalDAOImpl();
    }

    @Override
    public ExerciseDAO getExerciseDAO() {
        return new ExerciseDAOImpl();
    }

    @Override
    public TrainingDAO getTrainingDAO() {
        return new TrainingDAOImpl();
    }

    @Override
    public PlanDAO getPlanDAO() {
        return new PlanDAOImpl();
    }
}
