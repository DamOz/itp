package pl.damianozga.itp.dao;

import pl.damianozga.itp.model.Personal;

public interface PersonalDAO extends GenericDAO<Personal, Long> {

    Personal getPersonalById(long id);
}
