package pl.damianozga.itp.dao;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import pl.damianozga.itp.model.Plan;
import pl.damianozga.itp.model.Training;
import pl.damianozga.itp.model.User;
import pl.damianozga.itp.service.TrainingService;
import pl.damianozga.itp.util.ConnectionProvider;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PlanDAOImpl implements PlanDAO{

    private static final String CREATE_PLAN =
            "INSERT INTO plan (break, cardio, t1, t2, t3, t4, t5, t6, user_id) VALUES (:break, :cardio, :t1, :t2, :t3, :t4, :t5, :t6, :user_id);";

    private static final String READ_PLAN_BY_USER_ID =
            "SELECT id, break, cardio, t1, t2, t3, t4, t5, t6, plan.user_id FROM plan LEFT JOIN user ON plan.user_id =user.user_id WHERE plan.user_id=:user_id;";

    private NamedParameterJdbcTemplate template;

    public PlanDAOImpl() {
        template = new NamedParameterJdbcTemplate(ConnectionProvider.getDataSource());
    }

    @Override
    public Plan getPlanByUserId(long userId) {
        Plan resultPlan;
        SqlParameterSource parameterSource = new MapSqlParameterSource("user_id", userId);
        resultPlan = template.queryForObject(READ_PLAN_BY_USER_ID, parameterSource, new PlanRowMapper());
        return resultPlan;
    }



    @Override
    public Plan create(Plan newPlan) {
        Plan resultPlan = new Plan(newPlan);
        KeyHolder keyHolder = new GeneratedKeyHolder();
        Map<String, Object> paramMap = new HashMap<String, Object>();
        paramMap.put("break", resultPlan.getBreaks());
        paramMap.put("cardio", resultPlan.getCardio());
        paramMap.put("t1", resultPlan.getT1().getId());
        paramMap.put("t2", resultPlan.getT2().getId());
        paramMap.put("t3", resultPlan.getT3().getId());
        paramMap.put("t4", resultPlan.getT4().getId());
        paramMap.put("t5", resultPlan.getT5().getId());
        paramMap.put("t6", resultPlan.getT6().getId());
        paramMap.put("user_id", resultPlan.getUser().getId());
        SqlParameterSource parameterSource = new MapSqlParameterSource(paramMap);
        int update = template.update(CREATE_PLAN, parameterSource, keyHolder);
        if( update > 0) {
            resultPlan.setId(keyHolder.getKey().longValue());
        }
        return resultPlan;
    }



    @Override
    public Plan read(Long primaryKey) {
        return null;
    }

    @Override
    public boolean update(Plan updateObject) {
        return false;
    }

    @Override
    public boolean delete(Long key) {
        return false;
    }

    @Override
    public List<Plan> getAll() {
        return null;
    }

    private class PlanRowMapper implements RowMapper<Plan> {
        @Override
        public Plan mapRow(ResultSet resultSet, int i) throws SQLException {
            Plan plan = new Plan();
            User user = new User();
            plan.setUser(user);
            user.setId(resultSet.getLong("user_id"));
            plan.setId(resultSet.getLong("id"));
            plan.setBreaks(resultSet.getInt("break"));
            plan.setCardio(resultSet.getInt("cardio"));
            TrainingService trainingService = new TrainingService();
            List<Training> trainings = trainingService.getAllTrainingsByUserId(user.getId());
            plan.setT1(trainings.get(0));
            plan.setT2(trainings.get(1));
            plan.setT3(trainings.get(2));
            plan.setT4(trainings.get(3));
            plan.setT5(trainings.get(4));
            plan.setT6(trainings.get(5));
            return plan;
        }
    }
}
