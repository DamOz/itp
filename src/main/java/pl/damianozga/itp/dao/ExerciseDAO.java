package pl.damianozga.itp.dao;

import pl.damianozga.itp.model.Exercise;

import java.util.List;

public interface ExerciseDAO extends GenericDAO<Exercise, Long> {

    List<Exercise> getAll();
    Exercise getExerciseById(long id);
}
