package pl.damianozga.itp.converter;

public class PersonalConverter {


    // converters for summary page

    public String personalGoalConv(int goal) {
        String result = null;
        switch (goal) {
            case 1:
                result = "Zwiększenie masy ciała.";
                break;
            case 2:
                result = "Redukcja masy ciała.";
                break;
            case 3:
                result = "Zwiększenie wydolności.";
                break;
            case 4:
                result = "Ogólny rozwój.";
                break;
            case 5:
                result = "Zwiększenie siły.";
                break;
        }
        return result;
    }

    public String personalExpConv(int exp) {
        String result = null;
        switch (exp) {
            case 1:
                result = "Nigdy nie trenowałem.";
                break;
            case 2:
                result = "Trenowałem lata temu.";
                break;
            case 3:
                result = "Trenuje ale nie do końca wiem, co robię.";
                break;
            case 4:
                result = "Trenuje i znam ogólne zasady.";
                break;
            case 5:
                result = "Swobodnie wykonuje większość ćwiczeń.";
                break;
            case 6:
                result = "Mam duże doświadczenie, zrobię wszystko.";
                break;
        }
        return result;
    }

    public String personalInjuryConv(int injury) {
        String result;
        if (injury != 1) {
            result = "Kontuzjowany/osłabiony.";
        } else
            result = "Zdrowy!";
        return result;
    }

    public String personalItemConv(int item) {
        String result;
        if (item != 1) {
            result = "Mam dostęp.";
        } else
            result = "Nie mam dostępu";
        return result;
    }

    public String personalGymConv(int gym) {
        String result;
        if (gym != 1) {
            result = "Chodzę na siłownie";
        } else
            result = "Nie korzystam.";
        return result;
    }

    public String personalBodyConv(int body) {
        String result;
        if (body != 1) {
            result = "Tak";
        } else
            result = "Nie";
        return result;
    }

}
