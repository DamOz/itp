package pl.damianozga.itp.fullPlan;

import pl.damianozga.itp.model.Plan;
import pl.damianozga.itp.model.Training;
import pl.damianozga.itp.model.User;
import pl.damianozga.itp.service.TrainingService;

import java.util.ArrayList;

public class GeneralDevelopmentOneTrainingPlan extends Plan {

    private int planBreaks = 90;
    private int planCardio = 10;

    // creating a full plan from basic training

    public GeneralDevelopmentOneTrainingPlan createPlan(User user) {
        GeneralDevelopmentOneTrainingPlan resultPlan = new GeneralDevelopmentOneTrainingPlan();
        resultPlan.setBreaks(planBreaks);
        resultPlan.setCardio(planCardio);
        resultPlan.setT1(getFirstTraining(getAllUserTrainings(user)));
        Training[] trainingTab = createFinalTrainingsForPlan(getFirstTraining(getAllUserTrainings(user)));
        resultPlan.setT2(trainingTab[1]);
        resultPlan.setT3(trainingTab[2]);
        resultPlan.setT4(trainingTab[3]);
        resultPlan.setT5(trainingTab[4]);
        resultPlan.setT6(trainingTab[5]);
        resultPlan.setUser(user);
        return resultPlan;
    }

    // first training for calculation

    private ArrayList<Training> getAllUserTrainings(User user) {
        TrainingService trainingService = new TrainingService();
        ArrayList<Training> resultTrainingList = (ArrayList<Training>) trainingService.getAllTrainingsByUserId(user.getId());
        return resultTrainingList;
    }

    // getting first training for calculation

    private Training getFirstTraining(ArrayList<Training> trainings) {
        ArrayList<Training> allTrainings = trainings;
        Training resultTraining = allTrainings.get(0);
        return resultTraining;
    }

    // creating tab with updated trainings for full training plan

    private Training[] createFinalTrainingsForPlan(Training firstTraining) {
        Training[] resultTab = new Training[6];
        resultTab[0] = firstTraining;
        Training secondWeek = nextWeek(firstTraining);
        resultTab[1] = secondWeek;
        Training thirdWeek = nextWeek(secondWeek);
        resultTab[2] = thirdWeek;
        Training fourthWeek = thirdWeek;
        resultTab[3] = fourthWeek;
        Training fifthWeek = fourthWeek;
        resultTab[4] = fifthWeek;
        Training sixthWeek = nextWeek(fifthWeek);
        resultTab[5] = sixthWeek;
        return resultTab;
    }

    // updating repetitions in trainings for progression

    private Training nextWeek(Training weekBefore) {
        final int update = 2;
        Training nextWeekTraining = weekBefore;
        nextWeekTraining.setR1(weekBefore.getR1() + update);
        nextWeekTraining.setR2(weekBefore.getR2() + update);
        nextWeekTraining.setR3(weekBefore.getR3() + update);
        nextWeekTraining.setR4(weekBefore.getR4() + update);
        nextWeekTraining.setR5(weekBefore.getR5() + update);
        nextWeekTraining.setR6(weekBefore.getR6() + update);
        nextWeekTraining.setR7(weekBefore.getR7() + update);
        nextWeekTraining.setR8(weekBefore.getR8() + update);
        nextWeekTraining.setR9(weekBefore.getR9() + update);
        nextWeekTraining.setR10(weekBefore.getR10() + update);
        return nextWeekTraining;
    }
}
