package pl.damianozga.itp.model;

import java.util.Objects;

public class Personal {

    private long id;
    private int goal;
    private int exp;
    private int weight;
    private int training;
    private int neck;
    private int shoulder;
    private int elbow;
    private int hips;
    private int knees;
    private int ankles;
    private int gym;
    private int body;
    private int trx;
    private int kettle;
    private int dumb;
    private int bar;
    private int bosu;
    private int gum;

    public Personal() {
    }

    public Personal(Personal personal) {

        this.id = personal.id;
        this.goal = personal.goal;
        this.exp = personal.exp;
        this.weight = personal.weight;
        this.training = personal.training;
        this.neck = personal.neck;
        this.shoulder = personal.shoulder;
        this.elbow = personal.elbow;
        this.hips = personal.hips;
        this.knees = personal.knees;
        this.ankles = personal.ankles;
        this.gym = personal.gym;
        this.body = personal.body;
        this.trx = personal.trx;
        this.kettle = personal.kettle;
        this.dumb = personal.dumb;
        this.bar = personal.bar;
        this.bosu = personal.bosu;
        this.gum = personal.gum;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getGoal() {
        return goal;
    }

    public void setGoal(int goal) {
        this.goal = goal;
    }

    public int getExp() {
        return exp;
    }

    public void setExp(int exp) {
        this.exp = exp;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public int getTraining() {
        return training;
    }

    public void setTraining(int training) {
        this.training = training;
    }

    public int getNeck() {
        return neck;
    }

    public void setNeck(int neck) {
        this.neck = neck;
    }

    public int getShoulder() {
        return shoulder;
    }

    public void setShoulder(int shoulder) {
        this.shoulder = shoulder;
    }

    public int getElbow() {
        return elbow;
    }

    public void setElbow(int elbow) {
        this.elbow = elbow;
    }

    public int getHips() {
        return hips;
    }

    public void setHips(int hips) {
        this.hips = hips;
    }

    public int getKnees() {
        return knees;
    }

    public void setKnees(int knees) {
        this.knees = knees;
    }

    public int getAnkles() {
        return ankles;
    }

    public void setAnkles(int ankles) {
        this.ankles = ankles;
    }

    public int getGym() {
        return gym;
    }

    public void setGym(int gym) {
        this.gym = gym;
    }

    public int getBody() {
        return body;
    }

    public void setBody(int body) {
        this.body = body;
    }

    public int getTrx() {
        return trx;
    }

    public void setTrx(int trx) {
        this.trx = trx;
    }

    public int getKettle() {
        return kettle;
    }

    public void setKettle(int kettle) {
        this.kettle = kettle;
    }

    public int getDumb() {
        return dumb;
    }

    public void setDumb(int dumb) {
        this.dumb = dumb;
    }

    public int getBar() {
        return bar;
    }

    public void setBar(int bar) {
        this.bar = bar;
    }

    public int getBosu() {
        return bosu;
    }

    public void setBosu(int bosu) {
        this.bosu = bosu;
    }

    public int getGum() {
        return gum;
    }

    public void setGum(int gum) {
        this.gum = gum;
    }

    @Override
    public String toString() {
        return "Personal{" +
                "id=" + id +
                ", goal=" + goal +
                ", exp=" + exp +
                ", weight=" + weight +
                ", training=" + training +
                ", neck=" + neck +
                ", shoulder=" + shoulder +
                ", elbow=" + elbow +
                ", hips=" + hips +
                ", knees=" + knees +
                ", ankles=" + ankles +
                ", gym=" + gym +
                ", body=" + body +
                ", trx=" + trx +
                ", kettle=" + kettle +
                ", dumb=" + dumb +
                ", bar=" + bar +
                ", bosu=" + bosu +
                ", gum=" + gum +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Personal)) return false;
        Personal personal = (Personal) o;
        return getId() == personal.getId() &&
                getGoal() == personal.getGoal() &&
                getExp() == personal.getExp() &&
                getWeight() == personal.getWeight() &&
                getTraining() == personal.getTraining() &&
                getNeck() == personal.getNeck() &&
                getShoulder() == personal.getShoulder() &&
                getElbow() == personal.getElbow() &&
                getHips() == personal.getHips() &&
                getKnees() == personal.getKnees() &&
                getAnkles() == personal.getAnkles() &&
                getGym() == personal.getGym() &&
                getBody() == personal.getBody() &&
                getTrx() == personal.getTrx() &&
                getKettle() == personal.getKettle() &&
                getDumb() == personal.getDumb() &&
                getBar() == personal.getBar() &&
                getBosu() == personal.getBosu() &&
                getGum() == personal.getGum();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getGoal(), getExp(), getWeight(), getTraining(), getNeck(), getShoulder(),
                getElbow(), getHips(), getKnees(), getAnkles(), getGym(), getBody(), getTrx(),
                getKettle(), getDumb(), getBar(), getBosu(), getGum());
    }
}
