package pl.damianozga.itp.model;

import java.util.Objects;

public class Exercise {

    private long id;
    private String name;
    private int bodypart;
    private int dif;
    private int gym;
    private int item;
    private int injury;

    public Exercise() {}

    public Exercise(Exercise exercise) {
        this.id = exercise.id;
        this.name = exercise.name;
        this.bodypart = exercise.bodypart;
        this.dif = exercise.dif;
        this.gym = exercise.gym;
        this.item = exercise.item;
        this.injury = exercise.injury;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getBodypart() {
        return bodypart;
    }

    public void setBodypart(int bodypart) {
        this.bodypart = bodypart;
    }

    public int getDif() {
        return dif;
    }

    public void setDif(int dif) {
        this.dif = dif;
    }

    public int getGym() {
        return gym;
    }

    public void setGym(int gym) {
        this.gym = gym;
    }

    public int getItem() {
        return item;
    }

    public void setItem(int item) {
        this.item = item;
    }

    public int getInjury() {
        return injury;
    }

    public void setInjury(int injury) {
        this.injury = injury;
    }

    @Override
    public String toString() {
        return "Exercise{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", body_part=" + bodypart +
                ", dif=" + dif +
                ", gym=" + gym +
                ", item=" + item +
                ", injury=" + injury +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Exercise)) return false;
        Exercise exercise = (Exercise) o;
        return getId() == exercise.getId() &&
                getBodypart() == exercise.getBodypart() &&
                getDif() == exercise.getDif() &&
                getGym() == exercise.getGym() &&
                getItem() == exercise.getItem() &&
                getInjury() == exercise.getInjury() &&
                getName().equals(exercise.getName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getName(), getBodypart(), getDif(), getGym(), getItem(), getInjury());
    }
}
