package pl.damianozga.itp.model;

import java.util.Objects;

public class Training {

    private long id;
    private User user;
    private String e1;
    private String e2;
    private String e3;
    private String e4;
    private String e5;
    private String e6;
    private String e7;
    private String e8;
    private String e9;
    private String e10;
    private String e11;
    private String e12;
    private String e13;
    private String e14;
    private String e15;
    private String e16;
    private String e17;
    private String e18;
    private String e19;
    private String e20;
    private int r1;
    private int r2;
    private int r3;
    private int r4;
    private int r5;
    private int r6;
    private int r7;
    private int r8;
    private int r9;
    private int r10;
    private int r11;
    private int r12;
    private int r13;
    private int r14;
    private int r15;
    private int r16;
    private int r17;
    private int r18;
    private int r19;
    private int r20;
    private int s1;
    private int s2;
    private int s3;
    private int s4;
    private int s5;
    private int s6;
    private int s7;
    private int s8;
    private int s9;
    private int s10;
    private int s11;
    private int s12;
    private int s13;
    private int s14;
    private int s15;
    private int s16;
    private int s17;
    private int s18;
    private int s19;
    private int s20;


    public Training() {
    }

    public Training(Training training) {
        this.id = training.id;
        this.user = new User(training.user);
        this.e1 = training.e1;
        this.e2 = training.e2;
        this.e3 = training.e3;
        this.e4 = training.e4;
        this.e5 = training.e5;
        this.e6 = training.e6;
        this.e7 = training.e7;
        this.e8 = training.e8;
        this.e9 = training.e9;
        this.e10 = training.e10;
        this.e11 = training.e11;
        this.e12 = training.e12;
        this.e13 = training.e13;
        this.e14 = training.e14;
        this.e15 = training.e15;
        this.e16 = training.e16;
        this.e17 = training.e17;
        this.e18 = training.e18;
        this.e19 = training.e19;
        this.e20 = training.e20;
        this.r1 = training.r1;
        this.r2 = training.r2;
        this.r3 = training.r3;
        this.r4 = training.r4;
        this.r5 = training.r5;
        this.r6 = training.r6;
        this.r7 = training.r7;
        this.r8 = training.r8;
        this.r9 = training.r9;
        this.r10 = training.r10;
        this.r11 = training.r11;
        this.r12 = training.r12;
        this.r13 = training.r13;
        this.r14 = training.r14;
        this.r15 = training.r15;
        this.r16 = training.r16;
        this.r17 = training.r17;
        this.r18 = training.r18;
        this.r19 = training.r19;
        this.r20 = training.r20;
        this.s1 = training.s1;
        this.s2 = training.s2;
        this.s3 = training.s3;
        this.s4 = training.s4;
        this.s5 = training.s5;
        this.s6 = training.s6;
        this.s7 = training.s7;
        this.s8 = training.s8;
        this.s9 = training.s9;
        this.s10 = training.s10;
        this.s11 = training.s11;
        this.s12 = training.s12;
        this.s13 = training.s13;
        this.s14 = training.s14;
        this.s15 = training.s15;
        this.s16 = training.s16;
        this.s17 = training.s17;
        this.s18 = training.s18;
        this.s19 = training.s19;
        this.s20 = training.s20;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getE1() {
        return e1;
    }

    public void setE1(String e1) {
        this.e1 = e1;
    }

    public String getE2() {
        return e2;
    }

    public void setE2(String e2) {
        this.e2 = e2;
    }

    public String getE3() {
        return e3;
    }

    public void setE3(String e3) {
        this.e3 = e3;
    }

    public String getE4() {
        return e4;
    }

    public void setE4(String e4) {
        this.e4 = e4;
    }

    public String getE5() {
        return e5;
    }

    public void setE5(String e5) {
        this.e5 = e5;
    }

    public String getE6() {
        return e6;
    }

    public void setE6(String e6) {
        this.e6 = e6;
    }

    public String getE7() {
        return e7;
    }

    public void setE7(String e7) {
        this.e7 = e7;
    }

    public String getE8() {
        return e8;
    }

    public void setE8(String e8) {
        this.e8 = e8;
    }

    public String getE9() {
        return e9;
    }

    public void setE9(String e9) {
        this.e9 = e9;
    }

    public String getE10() {
        return e10;
    }

    public void setE10(String e10) {
        this.e10 = e10;
    }

    public String getE11() {
        return e11;
    }

    public void setE11(String e11) {
        this.e11 = e11;
    }

    public String getE12() {
        return e12;
    }

    public void setE12(String e12) {
        this.e12 = e12;
    }

    public String getE13() {
        return e13;
    }

    public void setE13(String e13) {
        this.e13 = e13;
    }

    public String getE14() {
        return e14;
    }

    public void setE14(String e14) {
        this.e14 = e14;
    }

    public String getE15() {
        return e15;
    }

    public void setE15(String e15) {
        this.e15 = e15;
    }

    public String getE16() {
        return e16;
    }

    public void setE16(String e16) {
        this.e16 = e16;
    }

    public String getE17() {
        return e17;
    }

    public void setE17(String e17) {
        this.e17 = e17;
    }

    public String getE18() {
        return e18;
    }

    public void setE18(String e18) {
        this.e18 = e18;
    }

    public String getE19() {
        return e19;
    }

    public void setE19(String e19) {
        this.e19 = e19;
    }

    public String getE20() {
        return e20;
    }

    public void setE20(String e20) {
        this.e20 = e20;
    }

    public int getR1() {
        return r1;
    }

    public void setR1(int r1) {
        this.r1 = r1;
    }

    public int getR2() {
        return r2;
    }

    public void setR2(int r2) {
        this.r2 = r2;
    }

    public int getR3() {
        return r3;
    }

    public void setR3(int r3) {
        this.r3 = r3;
    }

    public int getR4() {
        return r4;
    }

    public void setR4(int r4) {
        this.r4 = r4;
    }

    public int getR5() {
        return r5;
    }

    public void setR5(int r5) {
        this.r5 = r5;
    }

    public int getR6() {
        return r6;
    }

    public void setR6(int r6) {
        this.r6 = r6;
    }

    public int getR7() {
        return r7;
    }

    public void setR7(int r7) {
        this.r7 = r7;
    }

    public int getR8() {
        return r8;
    }

    public void setR8(int r8) {
        this.r8 = r8;
    }

    public int getR9() {
        return r9;
    }

    public void setR9(int r9) {
        this.r9 = r9;
    }

    public int getR10() {
        return r10;
    }

    public void setR10(int r10) {
        this.r10 = r10;
    }

    public int getR11() {
        return r11;
    }

    public void setR11(int r11) {
        this.r11 = r11;
    }

    public int getR12() {
        return r12;
    }

    public void setR12(int r12) {
        this.r12 = r12;
    }

    public int getR13() {
        return r13;
    }

    public void setR13(int r13) {
        this.r13 = r13;
    }

    public int getR14() {
        return r14;
    }

    public void setR14(int r14) {
        this.r14 = r14;
    }

    public int getR15() {
        return r15;
    }

    public void setR15(int r15) {
        this.r15 = r15;
    }

    public int getR16() {
        return r16;
    }

    public void setR16(int r16) {
        this.r16 = r16;
    }

    public int getR17() {
        return r17;
    }

    public void setR17(int r17) {
        this.r17 = r17;
    }

    public int getR18() {
        return r18;
    }

    public void setR18(int r18) {
        this.r18 = r18;
    }

    public int getR19() {
        return r19;
    }

    public void setR19(int r19) {
        this.r19 = r19;
    }

    public int getR20() {
        return r20;
    }

    public void setR20(int r20) {
        this.r20 = r20;
    }

    public int getS1() {
        return s1;
    }

    public void setS1(int s1) {
        this.s1 = s1;
    }

    public int getS2() {
        return s2;
    }

    public void setS2(int s2) {
        this.s2 = s2;
    }

    public int getS3() {
        return s3;
    }

    public void setS3(int s3) {
        this.s3 = s3;
    }

    public int getS4() {
        return s4;
    }

    public void setS4(int s4) {
        this.s4 = s4;
    }

    public int getS5() {
        return s5;
    }

    public void setS5(int s5) {
        this.s5 = s5;
    }

    public int getS6() {
        return s6;
    }

    public void setS6(int s6) {
        this.s6 = s6;
    }

    public int getS7() {
        return s7;
    }

    public void setS7(int s7) {
        this.s7 = s7;
    }

    public int getS8() {
        return s8;
    }

    public void setS8(int s8) {
        this.s8 = s8;
    }

    public int getS9() {
        return s9;
    }

    public void setS9(int s9) {
        this.s9 = s9;
    }

    public int getS10() {
        return s10;
    }

    public void setS10(int s10) {
        this.s10 = s10;
    }

    public int getS11() {
        return s11;
    }

    public void setS11(int s11) {
        this.s11 = s11;
    }

    public int getS12() {
        return s12;
    }

    public void setS12(int s12) {
        this.s12 = s12;
    }

    public int getS13() {
        return s13;
    }

    public void setS13(int s13) {
        this.s13 = s13;
    }

    public int getS14() {
        return s14;
    }

    public void setS14(int s14) {
        this.s14 = s14;
    }

    public int getS15() {
        return s15;
    }

    public void setS15(int s15) {
        this.s15 = s15;
    }

    public int getS16() {
        return s16;
    }

    public void setS16(int s16) {
        this.s16 = s16;
    }

    public int getS17() {
        return s17;
    }

    public void setS17(int s17) {
        this.s17 = s17;
    }

    public int getS18() {
        return s18;
    }

    public void setS18(int s18) {
        this.s18 = s18;
    }

    public int getS19() {
        return s19;
    }

    public void setS19(int s19) {
        this.s19 = s19;
    }

    public int getS20() {
        return s20;
    }

    public void setS20(int s20) {
        this.s20 = s20;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Training)) return false;
        Training training = (Training) o;
        return getId() == training.getId() &&
                getR1() == training.getR1() &&
                getR2() == training.getR2() &&
                getR3() == training.getR3() &&
                getR4() == training.getR4() &&
                getR5() == training.getR5() &&
                getR6() == training.getR6() &&
                getR7() == training.getR7() &&
                getR8() == training.getR8() &&
                getR9() == training.getR9() &&
                getR10() == training.getR10() &&
                getR11() == training.getR11() &&
                getR12() == training.getR12() &&
                getR13() == training.getR13() &&
                getR14() == training.getR14() &&
                getR15() == training.getR15() &&
                getR16() == training.getR16() &&
                getR17() == training.getR17() &&
                getR18() == training.getR18() &&
                getR19() == training.getR19() &&
                getR20() == training.getR20() &&
                getS1() == training.getS1() &&
                getS2() == training.getS2() &&
                getS3() == training.getS3() &&
                getS4() == training.getS4() &&
                getS5() == training.getS5() &&
                getS6() == training.getS6() &&
                getS7() == training.getS7() &&
                getS8() == training.getS8() &&
                getS9() == training.getS9() &&
                getS10() == training.getS10() &&
                getS11() == training.getS11() &&
                getS12() == training.getS12() &&
                getS13() == training.getS13() &&
                getS14() == training.getS14() &&
                getS15() == training.getS15() &&
                getS16() == training.getS16() &&
                getS17() == training.getS17() &&
                getS18() == training.getS18() &&
                getS19() == training.getS19() &&
                getS20() == training.getS20() &&
                Objects.equals(getUser(), training.getUser()) &&
                Objects.equals(getE1(), training.getE1()) &&
                Objects.equals(getE2(), training.getE2()) &&
                Objects.equals(getE3(), training.getE3()) &&
                Objects.equals(getE4(), training.getE4()) &&
                Objects.equals(getE5(), training.getE5()) &&
                Objects.equals(getE6(), training.getE6()) &&
                Objects.equals(getE7(), training.getE7()) &&
                Objects.equals(getE8(), training.getE8()) &&
                Objects.equals(getE9(), training.getE9()) &&
                Objects.equals(getE10(), training.getE10()) &&
                Objects.equals(getE11(), training.getE11()) &&
                Objects.equals(getE12(), training.getE12()) &&
                Objects.equals(getE13(), training.getE13()) &&
                Objects.equals(getE14(), training.getE14()) &&
                Objects.equals(getE15(), training.getE15()) &&
                Objects.equals(getE16(), training.getE16()) &&
                Objects.equals(getE17(), training.getE17()) &&
                Objects.equals(getE18(), training.getE18()) &&
                Objects.equals(getE19(), training.getE19()) &&
                Objects.equals(getE20(), training.getE20());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getUser(), getE1(), getE2(), getE3(), getE4(), getE5(), getE6(), getE7(), getE8(), getE9(), getE10(), getE11(), getE12(), getE13(), getE14(), getE15(), getE16(), getE17(), getE18(), getE19(), getE20(), getR1(), getR2(), getR3(), getR4(), getR5(), getR6(), getR7(), getR8(), getR9(), getR10(), getR11(), getR12(), getR13(), getR14(), getR15(), getR16(), getR17(), getR18(), getR19(), getR20(), getS1(), getS2(), getS3(), getS4(), getS5(), getS6(), getS7(), getS8(), getS9(), getS10(), getS11(), getS12(), getS13(), getS14(), getS15(), getS16(), getS17(), getS18(), getS19(), getS20());
    }

    @Override
    public String toString() {
        return "Training{" +
                "id=" + id +
                ", user=" + user +
                ", e1='" + e1 + '\'' +
                ", e2='" + e2 + '\'' +
                ", e3='" + e3 + '\'' +
                ", e4='" + e4 + '\'' +
                ", e5='" + e5 + '\'' +
                ", e6='" + e6 + '\'' +
                ", e7='" + e7 + '\'' +
                ", e8='" + e8 + '\'' +
                ", e9='" + e9 + '\'' +
                ", e10='" + e10 + '\'' +
                ", e11='" + e11 + '\'' +
                ", e12='" + e12 + '\'' +
                ", e13='" + e13 + '\'' +
                ", e14='" + e14 + '\'' +
                ", e15='" + e15 + '\'' +
                ", e16='" + e16 + '\'' +
                ", e17='" + e17 + '\'' +
                ", e18='" + e18 + '\'' +
                ", e19='" + e19 + '\'' +
                ", e20='" + e20 + '\'' +
                ", r1=" + r1 +
                ", r2=" + r2 +
                ", r3=" + r3 +
                ", r4=" + r4 +
                ", r5=" + r5 +
                ", r6=" + r6 +
                ", r7=" + r7 +
                ", r8=" + r8 +
                ", r9=" + r9 +
                ", r10=" + r10 +
                ", r11=" + r11 +
                ", r12=" + r12 +
                ", r13=" + r13 +
                ", r14=" + r14 +
                ", r15=" + r15 +
                ", r16=" + r16 +
                ", r17=" + r17 +
                ", r18=" + r18 +
                ", r19=" + r19 +
                ", r20=" + r20 +
                ", s1=" + s1 +
                ", s2=" + s2 +
                ", s3=" + s3 +
                ", s4=" + s4 +
                ", s5=" + s5 +
                ", s6=" + s6 +
                ", s7=" + s7 +
                ", s8=" + s8 +
                ", s9=" + s9 +
                ", s10=" + s10 +
                ", s11=" + s11 +
                ", s12=" + s12 +
                ", s13=" + s13 +
                ", s14=" + s14 +
                ", s15=" + s15 +
                ", s16=" + s16 +
                ", s17=" + s17 +
                ", s18=" + s18 +
                ", s19=" + s19 +
                ", s20=" + s20 +
                '}';
    }

    public Training createTraining(){
        return new Training();
    };
}
