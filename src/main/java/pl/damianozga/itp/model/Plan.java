package pl.damianozga.itp.model;

import java.util.Objects;

public class Plan {

    private long id;
    private int breaks;
    private int cardio;
    private Training t1;
    private Training t2;
    private Training t3;
    private Training t4;
    private Training t5;
    private Training t6;
    private User user;

    public Plan() {
    }

    public Plan(Plan plan) {
        this.id = plan.id;
        this.breaks = plan.breaks;
        this.cardio = plan.cardio;
        this.t1 = new Training(plan.t1);
        this.t2 = new Training(plan.t2);
        this.t3 = new Training(plan.t3);
        this.t4 = new Training(plan.t4);
        this.t5 = new Training(plan.t5);
        this.t6 = new Training(plan.t6);
        this.user = new User(plan.user);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getBreaks() {
        return breaks;
    }

    public void setBreaks(int breaks) {
        this.breaks = breaks;
    }

    public int getCardio() {
        return cardio;
    }

    public void setCardio(int cardio) {
        this.cardio = cardio;
    }

    public Training getT1() {
        return t1;
    }

    public void setT1(Training t1) {
        this.t1 = t1;
    }

    public Training getT2() {
        return t2;
    }

    public void setT2(Training t2) {
        this.t2 = t2;
    }

    public Training getT3() {
        return t3;
    }

    public void setT3(Training t3) {
        this.t3 = t3;
    }

    public Training getT4() {
        return t4;
    }

    public void setT4(Training t4) {
        this.t4 = t4;
    }

    public Training getT5() {
        return t5;
    }

    public void setT5(Training t5) {
        this.t5 = t5;
    }

    public Training getT6() {
        return t6;
    }

    public void setT6(Training t6) {
        this.t6 = t6;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "Plan{" +
                "id=" + id +
                ", breaks=" + breaks +
                ", cardio=" + cardio +
                ", t1=" + t1 +
                ", t2=" + t2 +
                ", t3=" + t3 +
                ", t4=" + t4 +
                ", t5=" + t5 +
                ", t6=" + t6 +
                ", user_id=" + user +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Plan)) return false;
        Plan plan = (Plan) o;
        return getId() == plan.getId() &&
                getBreaks() == plan.getBreaks() &&
                getCardio() == plan.getCardio() &&
                getUser() == plan.getUser() &&
                getT1().equals(plan.getT1()) &&
                Objects.equals(getT2(), plan.getT2()) &&
                Objects.equals(getT3(), plan.getT3()) &&
                Objects.equals(getT4(), plan.getT4()) &&
                Objects.equals(getT5(), plan.getT5()) &&
                Objects.equals(getT6(), plan.getT6());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getBreaks(), getCardio(), getT1(), getT2(), getT3(), getT4(), getT5(), getT6(), getUser());
    }

    public Plan createPlan(){
        return new Plan() {
        };
    };
}
