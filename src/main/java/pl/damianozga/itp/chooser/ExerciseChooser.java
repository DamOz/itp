package pl.damianozga.itp.chooser;

import pl.damianozga.itp.model.Exercise;
import pl.damianozga.itp.model.Personal;

import java.util.ArrayList;
import java.util.Random;


public class ExerciseChooser {



    public Exercise getExerciseCzworoglowy(ArrayList<Exercise> exercisesList, Personal personal){
        ArrayList<Exercise> tempExercises = exercisesList;
        ArrayList<Exercise> resultExercisesList = new ArrayList<Exercise>();
        Personal tempPersonal = personal;
        for(int i = 0; i < tempExercises.size(); i++ ){
            if(tempExercises.get(i).getBodypart() == 1 && tempExercises.get(i).getDif() <= tempPersonal.getExp()) {
                resultExercisesList.add(tempExercises.get(i));
                i++;
            }

        }
        Exercise exercise = this.randomPicker(resultExercisesList);
        return exercise;
    };

    public Exercise getExerciseDwuglowy(ArrayList<Exercise> exercisesList, Personal personal){
        ArrayList<Exercise> tempExercises = exercisesList;
        ArrayList<Exercise> resultExercisesList = new ArrayList<Exercise>();
        Personal tempPersonal = personal;
        for(int i = 0; i < tempExercises.size(); i++ ){
            if(tempExercises.get(i).getBodypart() == 1 && tempExercises.get(i).getDif() <= tempPersonal.getExp()) {
                resultExercisesList.add(tempExercises.get(i));
                i++;
            }

        }
        Exercise exercise = this.randomPicker(resultExercisesList);
        return exercise;
    };
    public Exercise getExerciseLydka(ArrayList<Exercise> exercisesList, Personal personal){
        ArrayList<Exercise> tempExercises = exercisesList;
        ArrayList<Exercise> resultExercisesList = new ArrayList<Exercise>();
        Personal tempPersonal = personal;
        for(int i = 0; i < tempExercises.size(); i++ ){
            if(tempExercises.get(i).getBodypart() == 1 && tempExercises.get(i).getDif() <= tempPersonal.getExp()) {
                resultExercisesList.add(tempExercises.get(i));
                i++;
            }

        }
        Exercise exercise = this.randomPicker(resultExercisesList);
        return exercise;
    };
    public Exercise getExercisePiszelowe(ArrayList<Exercise> exercisesList, Personal personal){
        ArrayList<Exercise> tempExercises = exercisesList;
        ArrayList<Exercise> resultExercisesList = new ArrayList<Exercise>();
        Personal tempPersonal = personal;
        for(int i = 0; i < tempExercises.size(); i++ ){
            if(tempExercises.get(i).getBodypart() == 1 && tempExercises.get(i).getDif() <= tempPersonal.getExp()) {
                resultExercisesList.add(tempExercises.get(i));
                i++;
            }

        }
        Exercise exercise = this.randomPicker(resultExercisesList);
        return exercise;
    };
    public Exercise getExercisePrzywodziciele(ArrayList<Exercise> exercisesList, Personal personal){
        ArrayList<Exercise> tempExercises = exercisesList;
        ArrayList<Exercise> resultExercisesList = new ArrayList<Exercise>();
        Personal tempPersonal = personal;
        for(int i = 0; i < tempExercises.size(); i++ ){
            if(tempExercises.get(i).getBodypart() == 1 && tempExercises.get(i).getDif() <= tempPersonal.getExp()) {
                resultExercisesList.add(tempExercises.get(i));
                i++;
            }

        }
        Exercise exercise = this.randomPicker(resultExercisesList);
        return exercise;
    };
    public Exercise getExercisePosladek(ArrayList<Exercise> exercisesList, Personal personal){
        ArrayList<Exercise> tempExercises = exercisesList;
        ArrayList<Exercise> resultExercisesList = new ArrayList<Exercise>();
        Personal tempPersonal = personal;
        for(int i = 0; i < tempExercises.size(); i++ ){
            if(tempExercises.get(i).getBodypart() == 1 && tempExercises.get(i).getDif() <= tempPersonal.getExp()) {
                resultExercisesList.add(tempExercises.get(i));
                i++;
            }

        }
        Exercise exercise = this.randomPicker(resultExercisesList);
        return exercise;
    };
    public Exercise getExerciseOdwodziciel(ArrayList<Exercise> exercisesList, Personal personal){
        ArrayList<Exercise> tempExercises = exercisesList;
        ArrayList<Exercise> resultExercisesList = new ArrayList<Exercise>();
        Personal tempPersonal = personal;
        for(int i = 0; i < tempExercises.size(); i++ ){
            if(tempExercises.get(i).getBodypart() == 1 && tempExercises.get(i).getDif() <= tempPersonal.getExp()) {
                resultExercisesList.add(tempExercises.get(i));
                i++;
            }

        }
        Exercise exercise = this.randomPicker(resultExercisesList);
        return exercise;
    };
    public Exercise getExerciseProstowniki(ArrayList<Exercise> exercisesList, Personal personal){
        ArrayList<Exercise> tempExercises = exercisesList;
        ArrayList<Exercise> resultExercisesList = new ArrayList<Exercise>();
        Personal tempPersonal = personal;
        for(int i = 0; i < tempExercises.size(); i++ ){
            if(tempExercises.get(i).getBodypart() == 1 && tempExercises.get(i).getDif() <= tempPersonal.getExp()) {
                resultExercisesList.add(tempExercises.get(i));
                i++;
            }

        }
        Exercise exercise = this.randomPicker(resultExercisesList);
        return exercise;
    };
    public Exercise getExerciseNajszerszyDol(ArrayList<Exercise> exercisesList, Personal personal){
        ArrayList<Exercise> tempExercises = exercisesList;
        ArrayList<Exercise> resultExercisesList = new ArrayList<Exercise>();
        Personal tempPersonal = personal;
        for(int i = 0; i < tempExercises.size(); i++ ){
            if(tempExercises.get(i).getBodypart() == 1 && tempExercises.get(i).getDif() <= tempPersonal.getExp()) {
                resultExercisesList.add(tempExercises.get(i));
                i++;
            }

        }
        Exercise exercise = this.randomPicker(resultExercisesList);
        return exercise;
    };
    public Exercise getExerciseNajszerszyGora(ArrayList<Exercise> exercisesList, Personal personal){
        ArrayList<Exercise> tempExercises = exercisesList;
        ArrayList<Exercise> resultExercisesList = new ArrayList<Exercise>();
        Personal tempPersonal = personal;
        for(int i = 0; i < tempExercises.size(); i++ ){
            if(tempExercises.get(i).getBodypart() == 1 && tempExercises.get(i).getDif() <= tempPersonal.getExp()) {
                resultExercisesList.add(tempExercises.get(i));
                i++;
            }

        }
        Exercise exercise = this.randomPicker(resultExercisesList);
        return exercise;
    };
    public Exercise getExerciseKaptury(ArrayList<Exercise> exercisesList, Personal personal){
        ArrayList<Exercise> tempExercises = exercisesList;
        ArrayList<Exercise> resultExercisesList = new ArrayList<Exercise>();
        Personal tempPersonal = personal;
        for(int i = 0; i < tempExercises.size(); i++ ){
            if(tempExercises.get(i).getBodypart() == 1 && tempExercises.get(i).getDif() <= tempPersonal.getExp()) {
                resultExercisesList.add(tempExercises.get(i));
                i++;
            }

        }
        Exercise exercise = this.randomPicker(resultExercisesList);
        return exercise;
    };
    public Exercise getExerciseBarkiPrzod(ArrayList<Exercise> exercisesList, Personal personal){
        ArrayList<Exercise> tempExercises = exercisesList;
        ArrayList<Exercise> resultExercisesList = new ArrayList<Exercise>();
        Personal tempPersonal = personal;
        for(int i = 0; i < tempExercises.size(); i++ ){
            if(tempExercises.get(i).getBodypart() == 1 && tempExercises.get(i).getDif() <= tempPersonal.getExp()) {
                resultExercisesList.add(tempExercises.get(i));
                i++;
            }

        }
        Exercise exercise = this.randomPicker(resultExercisesList);
        return exercise;
    };
    public Exercise getExerciseBarkiSrodek(ArrayList<Exercise> exercisesList, Personal personal){
        ArrayList<Exercise> tempExercises = exercisesList;
        ArrayList<Exercise> resultExercisesList = new ArrayList<Exercise>();
        Personal tempPersonal = personal;
        for(int i = 0; i < tempExercises.size(); i++ ){
            if(tempExercises.get(i).getBodypart() == 1 && tempExercises.get(i).getDif() <= tempPersonal.getExp()) {
                resultExercisesList.add(tempExercises.get(i));
                i++;
            }

        }
        Exercise exercise = this.randomPicker(resultExercisesList);
        return exercise;
    };
    public Exercise getExerciseBarkiTyl(ArrayList<Exercise> exercisesList, Personal personal){
        ArrayList<Exercise> tempExercises = exercisesList;
        ArrayList<Exercise> resultExercisesList = new ArrayList<Exercise>();
        Personal tempPersonal = personal;
        for(int i = 0; i < tempExercises.size(); i++ ){
            if(tempExercises.get(i).getBodypart() == 1 && tempExercises.get(i).getDif() <= tempPersonal.getExp()) {
                resultExercisesList.add(tempExercises.get(i));
                i++;
            }

        }
        Exercise exercise = this.randomPicker(resultExercisesList);
        return exercise;
    };
    public Exercise getExerciseBiceps(ArrayList<Exercise> exercisesList, Personal personal){
        ArrayList<Exercise> tempExercises = exercisesList;
        ArrayList<Exercise> resultExercisesList = new ArrayList<Exercise>();
        Personal tempPersonal = personal;
        for(int i = 0; i < tempExercises.size(); i++ ){
            if(tempExercises.get(i).getBodypart() == 1 && tempExercises.get(i).getDif() <= tempPersonal.getExp()) {
                resultExercisesList.add(tempExercises.get(i));
                i++;
            }

        }
        Exercise exercise = this.randomPicker(resultExercisesList);
        return exercise;
    };
    public Exercise getExerciseTriceps(ArrayList<Exercise> exercisesList, Personal personal){
        ArrayList<Exercise> tempExercises = exercisesList;
        ArrayList<Exercise> resultExercisesList = new ArrayList<Exercise>();
        Personal tempPersonal = personal;
        for(int i = 0; i < tempExercises.size(); i++ ){
            if(tempExercises.get(i).getBodypart() == 1 && tempExercises.get(i).getDif() <= tempPersonal.getExp()) {
                resultExercisesList.add(tempExercises.get(i));
                i++;
            }

        }
        Exercise exercise = this.randomPicker(resultExercisesList);
        return exercise;
    };
    public Exercise getExerciseKlatkaDol(ArrayList<Exercise> exercisesList, Personal personal){
        ArrayList<Exercise> tempExercises = exercisesList;
        ArrayList<Exercise> resultExercisesList = new ArrayList<Exercise>();
        Personal tempPersonal = personal;
        for(int i = 0; i < tempExercises.size(); i++ ){
            if(tempExercises.get(i).getBodypart() == 1 && tempExercises.get(i).getDif() <= tempPersonal.getExp()) {
                resultExercisesList.add(tempExercises.get(i));
                i++;
            }

        }
        Exercise exercise = this.randomPicker(resultExercisesList);
        return exercise;
    };
    public Exercise getExerciseKlatkaGora(ArrayList<Exercise> exercisesList, Personal personal){
        ArrayList<Exercise> tempExercises = exercisesList;
        ArrayList<Exercise> resultExercisesList = new ArrayList<Exercise>();
        Personal tempPersonal = personal;
        for(int i = 0; i < tempExercises.size(); i++ ){
            if(tempExercises.get(i).getBodypart() == 1 && tempExercises.get(i).getDif() <= tempPersonal.getExp()) {
                resultExercisesList.add(tempExercises.get(i));
                i++;
            }

        }
        Exercise exercise = this.randomPicker(resultExercisesList);
        return exercise;
    };
    public Exercise getExerciseBrzuchGora(ArrayList<Exercise> exercisesList, Personal personal){
        ArrayList<Exercise> tempExercises = exercisesList;
        ArrayList<Exercise> resultExercisesList = new ArrayList<Exercise>();
        Personal tempPersonal = personal;
        for(int i = 0; i < tempExercises.size(); i++ ){
            if(tempExercises.get(i).getBodypart() == 1 && tempExercises.get(i).getDif() <= tempPersonal.getExp()) {
                resultExercisesList.add(tempExercises.get(i));
                i++;
            }

        }
        Exercise exercise = this.randomPicker(resultExercisesList);
        return exercise;
    };
    public Exercise getExerciseBrzuchDol(ArrayList<Exercise> exercisesList, Personal personal){
        ArrayList<Exercise> tempExercises = exercisesList;
        ArrayList<Exercise> resultExercisesList = new ArrayList<Exercise>();
        Personal tempPersonal = personal;
        for(int i = 0; i < tempExercises.size(); i++ ){
            if(tempExercises.get(i).getBodypart() == 1 && tempExercises.get(i).getDif() <= tempPersonal.getExp()) {
                resultExercisesList.add(tempExercises.get(i));
                i++;
            }

        }
        Exercise exercise = this.randomPicker(resultExercisesList);
        return exercise;
    };
    public Exercise getExerciseBrzuchSkosy(ArrayList<Exercise> exercisesList, Personal personal){
        ArrayList<Exercise> tempExercises = exercisesList;
        ArrayList<Exercise> resultExercisesList = new ArrayList<Exercise>();
        Personal tempPersonal = personal;
        for(int i = 0; i < tempExercises.size(); i++ ){
            if(tempExercises.get(i).getBodypart() == 1 && tempExercises.get(i).getDif() <= tempPersonal.getExp()) {
                resultExercisesList.add(tempExercises.get(i));
                i++;
            }

        }
        Exercise exercise = this.randomPicker(resultExercisesList);
        return exercise;
    };
    public Exercise getExerciseCaleCialo(ArrayList<Exercise> exercisesList, Personal personal){
        ArrayList<Exercise> tempExercises = exercisesList;
        ArrayList<Exercise> resultExercisesList = new ArrayList<Exercise>();
        Personal tempPersonal = personal;
        for(int i = 0; i < tempExercises.size(); i++ ){
            if(tempExercises.get(i).getBodypart() == 1 && tempExercises.get(i).getDif() <= tempPersonal.getExp()) {
                resultExercisesList.add(tempExercises.get(i));
                i++;
            }

        }
        Exercise exercise = this.randomPicker(resultExercisesList);
        return exercise;
    };
    public Exercise getExerciseSkakanie(ArrayList<Exercise> exercisesList, Personal personal){
        ArrayList<Exercise> tempExercises = exercisesList;
        ArrayList<Exercise> resultExercisesList = new ArrayList<Exercise>();
        Personal tempPersonal = personal;
        for(int i = 0; i < tempExercises.size(); i++ ){
            if(tempExercises.get(i).getBodypart() == 1 && tempExercises.get(i).getDif() <= tempPersonal.getExp()) {
                resultExercisesList.add(tempExercises.get(i));
                i++;
            }

        }
        Exercise exercise = this.randomPicker(resultExercisesList);
        return exercise;
    };

    private Exercise randomPicker(ArrayList<Exercise> exercises){
        Random r = new Random();
        int randomIndex = r.nextInt(exercises.size());
        Exercise resultExercise = exercises.get(randomIndex);
        return resultExercise;
    }
}
