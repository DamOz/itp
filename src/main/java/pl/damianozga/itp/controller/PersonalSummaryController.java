package pl.damianozga.itp.controller;

import pl.damianozga.itp.converter.PersonalConverter;
import pl.damianozga.itp.model.Personal;
import pl.damianozga.itp.model.User;
import pl.damianozga.itp.service.PersonalService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet("/summary")
public class PersonalSummaryController extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try{
            getPersonalObject(req);
        } catch (SQLException e) {
            e.printStackTrace();
            resp.sendError(403);
        }
        req.getRequestDispatcher("summary.jsp").forward(req, resp);
    }

    public void getPersonalObject(HttpServletRequest request) throws  SQLException {
        PersonalService personalService = new PersonalService();
        User tempUser;
        tempUser = (User) request.getSession().getAttribute("user");
        long id = tempUser.getId();
        Personal tempPersonal = personalService.getPersonalById(id);
        request.setAttribute("personal", tempPersonal);
        PersonalConverter pc = new PersonalConverter();
        request.setAttribute("conv", pc);
    }

}
