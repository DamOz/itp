package pl.damianozga.itp.controller;

import pl.damianozga.itp.fullPlan.GeneralDevelopmentOneTrainingPlan;
import pl.damianozga.itp.model.Exercise;
import pl.damianozga.itp.model.Personal;
import pl.damianozga.itp.model.Training;
import pl.damianozga.itp.model.User;
import pl.damianozga.itp.service.ExerciseService;
import pl.damianozga.itp.service.PersonalService;
import pl.damianozga.itp.service.TrainingService;
import pl.damianozga.itp.submodels.generalDevelopment.GeneralDevelopmentOneTraining;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.sql.SQLException;
import java.util.ArrayList;

@WebServlet("/training")
public class TrainingController extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if(trainingStatus(req) == true){
            try {
                getTrainingObject(req);
            } catch (SQLException e) {
                e.printStackTrace();

            }
            req.getRequestDispatcher("training.jsp").forward(req, resp);
        } else {
            createTraining(req, resp);
            try {
                getTrainingObject(req);
            } catch (SQLException e) {
                e.printStackTrace();

            }
            req.getRequestDispatcher("training.jsp").forward(req, resp);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // usuwanie planu

        // idziemy do generowania planu

    }




    private void createTraining(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        User tempUser = (User) req.getSession().getAttribute("user");
        PersonalService personalService = new PersonalService();
        Personal tempPersonal = personalService.getPersonalById(tempUser.getId());
        ExerciseService exerciseService = new ExerciseService();
        ArrayList<Exercise> tempExercises = (ArrayList<Exercise>) exerciseService.getAllExercises();
        GeneralDevelopmentOneTraining gdot = new GeneralDevelopmentOneTraining();
        Training training = gdot.createTraining(tempUser, tempExercises, tempPersonal);
        TrainingService trainingService = new TrainingService();
        trainingService.addPreperedTraining(training);
    }

    private void getTrainingObject(HttpServletRequest request) throws SQLException {
        User tempUser;
        tempUser = (User) request.getSession().getAttribute("user");
        long id = tempUser.getId();
        TrainingService trainingService = new TrainingService();
        Training tempTraining = trainingService.getTrainingById(id);
        request.setAttribute("training", tempTraining);
        request.setAttribute("user", tempUser);

    }

    private boolean trainingStatus(HttpServletRequest req){
        long id = getUserId(req);
        TrainingService trainingService = new TrainingService();
        Training tempTraining = trainingService.getTrainingById(id);
        boolean change;
        if(tempTraining != null){
            change = true;
        } else
            change = false;
        return change;
    }

    private long getUserId(HttpServletRequest req) {
        User tempUser = (User) req.getSession().getAttribute("user");
        long id = tempUser.getId();
        return id;
    }

}
