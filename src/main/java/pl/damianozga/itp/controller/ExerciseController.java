package pl.damianozga.itp.controller;

import pl.damianozga.itp.service.ExerciseService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/exercise")
public class ExerciseController extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("exercise.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        createExerciseFromForm(req, resp);
    }

    private void createExerciseFromForm(HttpServletRequest req, HttpServletResponse resp) throws ServletException,
            IOException {
        req.setCharacterEncoding("UTF-8");
        String name = req.getParameter("inputName");
        int bodypart = Integer.parseInt(req.getParameter("inputBodypart"));
        int dif = Integer.parseInt(req.getParameter("inputDif"));
        int gym = Integer.parseInt(req.getParameter("inputGym"));
        int item = Integer.parseInt(req.getParameter("inputItem"));
        int injury = Integer.parseInt(req.getParameter("inputInjury"));
        ExerciseService exerciseService = new ExerciseService();
        exerciseService.addExercise(name, bodypart, dif, gym, item, injury);
        resp.sendRedirect(req.getContextPath() + "/");
    }
}
