package pl.damianozga.itp.controller;

import pl.damianozga.itp.model.Personal;
import pl.damianozga.itp.model.User;
import pl.damianozga.itp.parser.IntegerParser;
import pl.damianozga.itp.service.PersonalService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/personal")
public class PersonalController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if(personalStatus(req) == true) {
            req.getRequestDispatcher("/summary").forward(req, resp);
        } else
        req.getRequestDispatcher("personal.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        createPersonalFromForm(req, resp);
        resp.sendRedirect(req.getContextPath() + "/summary");
    }

    private void createPersonalFromForm(HttpServletRequest req, HttpServletResponse resp) throws ServletException,
            IOException {
        req.setCharacterEncoding("UTF-8");
        IntegerParser iP = new IntegerParser();
        long id = getUserId(req);
        int goal = Integer.parseInt(req.getParameter("inputGoal"));
        int exp = Integer.parseInt(req.getParameter("inputExp"));
        int weight = Integer.parseInt(req.getParameter("inputWeight"));
        int training = Integer.parseInt(req.getParameter("inputTraining"));
        String pNeck = req.getParameter("inputNeck");
        int neck = iP.integerNullParser(pNeck);
        String pShoulder = req.getParameter("inputShoulder");
        int shoulder = iP.integerNullParser(pShoulder);
        String pElbow = req.getParameter("inputElbow");
        int elbow = iP.integerNullParser(pElbow);
        String pHips = req.getParameter("inputHips");
        int hips = iP.integerNullParser(pHips);
        String pKnees = req.getParameter("inputKnees");
        int knees = iP.integerNullParser(pKnees);
        String pAnkles = req.getParameter("inputAnkles");
        int ankles = iP.integerNullParser(pAnkles);
        String pGym = req.getParameter("inputGym");
        int gym = iP.integerNullParser(pGym);
        String pBody = req.getParameter("inputBody");
        int body = iP.integerNullParser(pBody);
        String pTrx = req.getParameter("inputTrx");
        int trx = iP.integerNullParser(pTrx);
        String pKettle = req.getParameter("inputKettle");
        int kettle = iP.integerNullParser(pKettle);
        String pDumb = req.getParameter("inputDumb");
        int dumb = iP.integerNullParser(pDumb);
        String pBar = req.getParameter("inputBar");
        int bar = iP.integerNullParser(pBar);
        String pBosu = req.getParameter("inputBosu");
        int bosu = iP.integerNullParser(pBosu);
        String pGum = req.getParameter("inputGum");
        int gum = iP.integerNullParser(pGum);
        PersonalService personalService = new PersonalService();
        personalService.addPersonal( id, goal, exp, weight,  training, neck, shoulder, elbow, hips,
                knees, ankles, gym, body, trx, kettle, dumb, bar, bosu, gum);
        resp.sendRedirect(req.getContextPath() + "/");
    }

    private boolean personalStatus(HttpServletRequest req) {
        long id = getUserId(req);
        PersonalService personalService = new PersonalService();
        Personal tempPersonal = personalService.getPersonalById(id);
        boolean change;
        if(tempPersonal != null){
            change = true;
        } else
            change = false;
        return change;
    }

    private long getUserId(HttpServletRequest req) {
        User tempUser = (User) req.getSession().getAttribute("user");
        long id = tempUser.getId();
        return id;
    }
}
