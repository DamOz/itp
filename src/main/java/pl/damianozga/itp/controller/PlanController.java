package pl.damianozga.itp.controller;

import org.springframework.dao.EmptyResultDataAccessException;
import pl.damianozga.itp.fullPlan.GeneralDevelopmentOneTrainingPlan;
import pl.damianozga.itp.model.Plan;
import pl.damianozga.itp.model.Training;
import pl.damianozga.itp.model.User;
import pl.damianozga.itp.service.PlanService;
import pl.damianozga.itp.service.TrainingService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet("/plan")
public class PlanController extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
       try {
           if (planStatus(req) == true) {
               getPlanObject(req);
               req.getRequestDispatcher("plan.jsp").forward(req, resp);
           } else {
               getPlanObject(req);
               req.getRequestDispatcher("plan.jsp").forward(req, resp);
           }
       } catch (EmptyResultDataAccessException e) {
           createFullPlan(req, resp);
           req.getRequestDispatcher("/plan").forward(req, resp);
       }
    }

    private void getPlanObject(HttpServletRequest request) throws ServletException, IOException {
        User tempUser;
        tempUser = (User) request.getSession().getAttribute("user");
        Plan tempPlan = getPlanByService(request);
        Training[] tempTrainingsTab = createTabOfFinalTrainings(tempPlan);
        request.setAttribute("plan", tempPlan);
        request.setAttribute("user", tempUser);
        request.setAttribute("trainingTab", tempTrainingsTab);

    }

    private Training[] createTabOfFinalTrainings(Plan planObject){
        Training[] trainings = new Training[6];
        trainings[0] = planObject.getT1();
        trainings[1] = planObject.getT2();
        trainings[2] = planObject.getT3();
        trainings[3] = planObject.getT4();
        trainings[4] = planObject.getT5();
        trainings[5] = planObject.getT6();
        return trainings;
    }

    private void createFullPlan(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        User tempUser = (User) req.getSession().getAttribute("user");
        PlanService planService = new PlanService();
        GeneralDevelopmentOneTrainingPlan GDOTP = new GeneralDevelopmentOneTrainingPlan();
        GeneralDevelopmentOneTrainingPlan tempGDOTP = GDOTP.createPlan(tempUser);
        TrainingService trainingService = new TrainingService();
        trainingService.addPreperedTraining(tempGDOTP.getT1());
        trainingService.addPreperedTraining(tempGDOTP.getT2());
        trainingService.addPreperedTraining(tempGDOTP.getT3());
        trainingService.addPreperedTraining(tempGDOTP.getT4());
        trainingService.addPreperedTraining(tempGDOTP.getT5());
        trainingService.addPreperedTraining(tempGDOTP.getT6());
        planService.addPlan(tempGDOTP.getBreaks(), tempGDOTP.getCardio(), tempGDOTP.getT1(), tempGDOTP.getT2(), tempGDOTP.getT3(),tempGDOTP.getT4(),tempGDOTP.getT5(),tempGDOTP.getT6(), tempGDOTP.getUser());
    }

    private boolean planStatus(HttpServletRequest req) throws ServletException, IOException {
        Plan tempPlan = getPlanByService(req);
        boolean change;
        if(tempPlan != null){
            change = true;
        } else
            change = false;
        return change;
    }

    private Plan getPlanByService(HttpServletRequest req) throws ServletException, IOException {
        long id = getUserId(req);
        PlanService planService = new PlanService();
        Plan tempPlan = planService.getPlanByUserId(id);
        return tempPlan;
    }

    private long getUserId(HttpServletRequest req) {
        User tempUser = (User) req.getSession().getAttribute("user");
        long id = tempUser.getId();
        return id;
    }
}
