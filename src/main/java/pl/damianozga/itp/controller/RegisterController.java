package pl.damianozga.itp.controller;


import pl.damianozga.itp.service.UserService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/register")
public class RegisterController extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("register.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        createUserFromForm(req, resp);
            }

    private void createUserFromForm(HttpServletRequest req, HttpServletResponse resp) throws ServletException,
            IOException {
        req.setCharacterEncoding("UTF-8");
        String name = req.getParameter("inputName");
        String email = req.getParameter("inputEmail");
        String password = req.getParameter("inputPassword");
        UserService userService = new UserService();
        userService.addUser(name, email, password);
        resp.sendRedirect(req.getContextPath() + "/");
    }
}
