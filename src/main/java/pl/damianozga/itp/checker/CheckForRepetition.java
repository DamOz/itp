package pl.damianozga.itp.checker;

import pl.damianozga.itp.chooser.ExerciseChooser;
import pl.damianozga.itp.model.Exercise;
import pl.damianozga.itp.model.Personal;

import java.util.ArrayList;

public class CheckForRepetition {

    public String checkingForRepetition(String e1, String e2, ArrayList<Exercise> exercisesList, Personal personal) {
        ExerciseChooser chooser = new ExerciseChooser();
        while (!e2.equals(e1)){
            e2 = chooser.getExerciseCzworoglowy(exercisesList, personal).getName();
        }
        return e2;
    };

    public String checkingForRepetition3(String e1, String e2, String e3, ArrayList<Exercise> exercisesList, Personal personal) {
        ExerciseChooser chooser = new ExerciseChooser();
        while (!e3.equals(e1) && !e3.equals(e2)){
            e3 = chooser.getExerciseCzworoglowy(exercisesList, personal).getName();
        }
        return e3;
    };

    public String checkingForRepetition4(String e1, String e2, String e3, String e4, ArrayList<Exercise> exercisesList, Personal personal) {
        ExerciseChooser chooser = new ExerciseChooser();
        while (!e4.equals(e3) && !e4.equals(e2) && !e4.equals(e1)){
            e3 = chooser.getExerciseCzworoglowy(exercisesList, personal).getName();
        }
        return e3;
    };
}
