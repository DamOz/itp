<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!-- navigation -->
<section>
    <nav class="navbar navbar-expand-md text-uppercase justify-content-end mb-2 bg-dark">
        <div class="d-none d-xl-block">
            <a class="navbar" href="${pageContext.request.contextPath}/">
                <p>Logo</p>
            </a>
        </div>
        <div class="container-fluid justify-content-end">
            <button class="navbar-toggler" type="button">
                <p>Hamburger</p>
            </button>
        </div>
        <!-- nav links -->
        <div class="collapse navbar-collapse text-uppercase justify-content-end">
            <ul class="navbar-nav">
                <li class="nav-item text-wrap">
                    <a class="nav-link" href="${pageContext.request.contextPath}/">Strona główna</a>
                </li>


                <c:choose>
                    <c:when test="${not empty sessionScope.user}">
                        <li class="nav-item text-wrap">
                            <a class="nav-link" href="${pageContext.request.contextPath}/personal">Stwórz plan</a>
                        </li>
                        <li class="nav-item text-wrap">
                            <a class="nav-link" href="${pageContext.request.contextPath}/exercise">Dodaj ćwiczenia</a>
                        </li>
                        <li class="nav-item text-wrap">
                            <a class="nav-link" href="${pageContext.request.contextPath}/plan">Twój plan</a>
                        </li>
                        <li class="nav-item text-wrap">
                            <a class="nav-link" href="${pageContext.request.contextPath}/logout">Wyloguj się</a>
                        </li>
                    </c:when>
                    <c:otherwise>
                        <li class="nav-item text-wrap">
                            <a class="nav-link" href="${pageContext.request.contextPath}/login">Zaloguj się</a>
                        </li>
                        <li class="nav-item text-wrap">
                            <a class="nav-link" href="${pageContext.request.contextPath}/register">Zarejestruj się</a>
                        </li>
                    </c:otherwise>
                </c:choose>
            </ul>
        </div>
    </nav>
</section>